import datetime
import json

from django.db import connection
from django.http import HttpResponse
from django.shortcuts import render


def admin_get_all_pesanan():
    print("ADMIN>>>")
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM toys_rent.PEMESANAN")
        data = cursor.fetchall()
        cursor.close()

    return data


def anggota_get_all_pesanan(id_user):
    print("ANGGOTA>>>")
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to toys_rent;")
        cursor.execute("SELECT * FROM PEMESANAN WHERE no_ktp_pemesan = '{}';".format(id_user))
        data = cursor.fetchall()
        cursor.close()

    return data


def get_barang_from_id_pesanan(id_pesanan):
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO toys_rent;")
        cursor.execute("SELECT B.id_barang, B.nama_item FROM PEMESANAN P, BARANG_PESANAN BP, BARANG B " +
                       "WHERE P.id_pemesanan = BP.id_pemesanan AND B.id_barang = BP.id_barang " +
                       "AND P.id_pemesanan = '{}';".format(id_pesanan))

        print("= AFTER QUERY")  # LOG
        data = cursor.fetchall()
        cursor.close()

    return data


def berhasil_pesan(request):
    idn = request.session.get('idn')
    now = datetime.datetime.now()
    tanggal = now.strftime("%Y-%m-%d %H:%M")

    if request.method == 'POST':
        data = json.loads(request.POST['pesanan'])
        with connection.cursor() as cursor:
            cursor.execute("SET SEARCH_PATH TO toys_rent;")
            cursor.execute("SELECT id_pemesanan FROM PEMESANAN ORDER BY id_pemesanan DESC")
            id_pemesanan = int(cursor.fetchone()[0]) + 1

        kuantitas_barang = str(len(data))
        totalnya = request.POST['total_harga']

        print("<<< LOG >>>")

        print(id_pemesanan)
        # print("TOTALLLLLLLLALAALSDLALSDASLD")
        # print(totalnya)

        ####
        print(request.POST['pesanan'])
        semua = request.POST['pesanan']
        semua = json.loads(semua)

        ####

        with connection.cursor() as cursor:
            cursor.execute("SET SEARCH_PATH TO toys_rent;")
            cursor.execute(
                "INSERT INTO PEMESANAN VALUES ('{}', '{}', {}, {}, {}, '{}','{}');".format(id_pemesanan, tanggal,
                                                                                           kuantitas_barang, totalnya,
                                                                                           1234, idn, 'Mauv'))
        for i in semua:
            print(i[1])
            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO toys_rent;")
                cursor.execute("SELECT COUNT(*) FROM BARANG_PESANAN;")
                jumlah = cursor.fetchone()[0] + 1
                cursor.execute(
                    "INSERT INTO BARANG_PESANAN VALUES ('{}', {}, '{}', '2019-05-28', 2, '2019-05-30', 'Mauv')".format(
                        id_pemesanan, jumlah, i[1]))
                cursor.close()

        return HttpResponse("Berhasil POST")
    else:

        return render(request, 'berhasilpesan.html')


def get_all_barang():
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO toys_rent;")
        cursor.execute("SELECT * FROM BARANG;")
        all_barang = cursor.fetchall()
        return all_barang


def get_all_user():
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO toys_rent;")
        cursor.execute("SELECT P.nama_lengkap, P.no_ktp FROM ANGGOTA A, PENGGUNA P WHERE P.no_ktp = A.no_ktp;")
        all_anggota = cursor.fetchall()
        return all_anggota


# Create your views here.
def index(request):
    role = request.session.get('role')
    idn = request.session.get('idn')
    print("role : " + role)
    all_barang = get_all_barang()
    all_anggota = get_all_user()

    context = {
        'all_barang': all_barang,
        'all_anggota': all_anggota,
        'idn': idn,
        'role': role

    }
    return render(request, 'pemesanan.html', context)


def get_harga_of_barang(id_barang):
    with connection.cursor() as cursor:
        cursor.execute("SET search_path TO toys_rent")
        cursor.execute(
            "SELECT harga_sewa FROM INFO_BARANG_LEVEL NATURAL JOIN BARANG WHERE id_barang = '{}';".format(
                id_barang))
        hasil = cursor.fetchone()[0]
    return hasil


def cek_total_harga(request):
    if request.method == 'POST':
        print(request.POST['pesanan'])
        semua = request.POST['pesanan'].split(",")
        total = 0

        # for i in semua:
        #     print(i)
        #     print(get_harga_of_barang(i))

        for i in range(len(semua) - 1):
            harga = get_harga_of_barang(semua[i])
            print(harga)
            total += int(harga)

        print("TOTAL")
        print(str(total))

        return HttpResponse(total)


def daftar_pesanan(request):
    if request.method == 'POST':
        id_pesanan = request.POST['id_pesanan']
        print(id_pesanan)

        with connection.cursor() as cursor:
            cursor.execute("SET search_path TO toys_rent")
            cursor.execute("DELETE FROM PEMESANAN WHERE LOWER(id_pemesanan) = LOWER('{}');".format(id_pesanan))
        return HttpResponse('SUCCESS')
    else:
        low = 0
        high = 0

        role = request.session.get('role')
        idn = request.session.get('idn')
        print('role: ' + role)
        if role == 'admin':
            semua_pesanan = admin_get_all_pesanan()
        elif role == 'member':
            semua_pesanan = anggota_get_all_pesanan(idn)
        else:
            return HttpResponse('<h3>gaboleh buka kalo belom login<h3>')

        for i in range(len(semua_pesanan)):
            semua_pesanan[i] += (get_barang_from_id_pesanan(semua_pesanan[i][0]),)

        context = {
            'semua_pesanan': semua_pesanan,
            'idn': idn,
            'role': role

        }
        return render(request, 'daftar-pesanan.html', context)


def update_pesanan(request, id_pesanan):
    if request.method == 'POST':
        if request.POST['command'] == 'TAMBAH':
            print('>> ' + request.POST['the_barang'])
            id_barang = request.POST['the_barang'].split(',')[0][2:-1]

            print('<<< ' + id_barang)

            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO toys_rent;")
                cursor.execute("SELECT COUNT(*) FROM BARANG_PESANAN;")
                jumlah = cursor.fetchone()[0] + 1
                cursor.execute(
                    "INSERT INTO BARANG_PESANAN VALUES ('{}', {}, '{}', '2019-05-28', 2, '2019-05-30', 'Puce')".format(
                        id_pesanan, jumlah, id_barang))
                cursor.close()

            return HttpResponse('success TAMBAH')
        elif request.POST['command'] == 'DELETE':
            id_barang = request.POST['the_barang']
            print(id_barang)
            with connection.cursor() as cursor:
                cursor.execute("SET SEARCH_PATH TO toys_rent;")
                cursor.execute(
                    "DELETE FROM BARANG_PESANAN WHERE id_barang = '{}' AND id_pemesanan = '{}';".format(id_barang,
                                                                                                        id_pesanan))
            cursor.close()

            return HttpResponse('success DELETE')

    else:
        with connection.cursor() as cursor:
            cursor.execute("SET SEARCH_PATH TO toys_rent;")
            cursor.execute("SELECT * FROM BARANG")
            barang = cursor.fetchall()
            cursor.close()

        data = get_barang_from_id_pesanan(id_pesanan)

        context = {
            'data': data,
            'barang': barang,
        }
        print(data)
        return render(request, 'update-pesanan.html', context)
