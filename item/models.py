from django.db import models

# Create your models here.
class Kategori(models.Model):
    nama = models.CharField(max_length=100, primary_key=True)
    level = models.IntegerField()
    sub = models.ForeignKey("Kategori", on_delete= models.CASCADE, blank=True, null=True)

    class Meta:
        verbose_name_plural = "Kategori"

    def __str__(self):
        return self.nama

    def __unicode__(self):
        return self.nama
