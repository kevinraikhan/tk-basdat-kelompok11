from django import forms
from .models import Kategori


class MyModelMultipleChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return obj.nama

class ItemForm(forms.Form):
    nama = forms.CharField(label = "Nama", max_length = 50, widget = forms.TextInput(
        attrs = {
            "required" : True,
            "class" : "form-control",
            "placeholder" : "Nama Item"
        }
    ))
    deskripsi = forms.CharField(label = "Deskripsi", max_length = 50, widget = forms.Textarea(
        attrs = {
            "rows":"5",
            "required" : True,
            "class" : "form-control",
            "placeholder" : "Deskripsi"
        }
    ))
    usia_awal = forms.FloatField(label = "", widget = forms.NumberInput(
        attrs = {
            "required" : True,
            "class" : "form-control-1",
            "placeholder" : "Usia Awal"
        }
    ))
    usia_akhir = forms.FloatField(label = "", widget = forms.NumberInput(
        attrs = {
            "required" : True,
            "class" : "form-control-1",
            "placeholder" : "Usia Akhir"
        }
    ))
    bahan = forms.CharField(label = "Bahan", max_length = 50, widget = forms.TextInput(
        attrs = {
            "required" : True,
            "class" : "form-control",
            "placeholder" : "Bahan"
        }
    ))
    kategori = MyModelMultipleChoiceField(
        queryset = Kategori.objects.all(),
        label = "Kategori",
        widget = forms.SelectMultiple(
            attrs = {
                "class": "selectpicker form-control",
                "placeholder" : "Kategori",
                "multiple": "multiple",
                "data-live-search": "true"
            }
        )
    )


