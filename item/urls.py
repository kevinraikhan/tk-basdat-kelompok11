from django.urls import path
from .views import *

app_name = 'item'

urlpatterns = [
    path('<str:updated>', show_items, name = 'show_items'),
    path('create/', create_item, name = 'create_item'),
    path('update/<str:item_name>/', update_item, name = 'update_item'),
    path('delete/', delete_item, name = 'delete_item'),
]
