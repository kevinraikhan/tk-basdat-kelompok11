from django.db import connection
from django.http import JsonResponse
from django.shortcuts import render, redirect
import psycopg2
import json

from .forms import ItemForm

def dictfetchall(cursor):
    "Returns all rows from a cursor as a dict"
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]

def get_items():
    with connection.cursor() as control:
        control.execute("select * from toys_rent.item")
        return dictfetchall(control)

def get_item(idn):
    with connection.cursor() as control:
        control.execute("select * from toys_rent.item where nama = %s", (idn,))
        return dictfetchall(control)

def get_item_categories():
    with connection.cursor() as control:
        control.execute("select * from toys_rent.kategori_item")
        return dictfetchall(control)

def get_item_category(idn):
    with connection.cursor() as control:
        control.execute("select * from toys_rent.kategori_item where nama_item = %s", (idn,))
        return dictfetchall(control)

def get_categories():
    with connection.cursor() as control:
        control.execute("select * from toys_rent.kategori")
        return dictfetchall(control)

def show_items(req, updated):
    if 'role' not in req.session:
        return redirect("toysrent_auth:login")
        
    items = get_items()
    item_categories = get_item_categories()

    for item in items:
        item['kategori'] = []

    for item_category in item_categories:
        for item in items:
            if item_category['nama_item'] == item['nama']:
                item['kategori'].append(item_category['nama_kategori'])
                break
        
    if updated == "new":
        updated = True
    else:
        updated = False

    return render(req, 'items.html', {"role" : req.session['role'], "items" : items, "updated": updated})

def create_item(req):
    if 'role' not in req.session:
        return redirect("toysrent_auth:login")
    
    if req.session['role'] != "admin":
        return redirect("user:profile")

    if req.method == 'POST':
        dict_req = dict(req.POST)
        
        with connection.cursor() as control:
            control.execute("insert into toys_rent.item(nama, deskripsi, usia_dari, usia_sampai, bahan)" +
                            "values(%s,%s,%s,%s,%s)",
                            (dict_req['nama'][0], dict_req['deskripsi'][0], int(dict_req['usia_awal'][0]), int(dict_req['usia_akhir'][0]), dict_req['bahan'][0],))
            
            for kategori in dict_req['kategori']:
                control.execute("insert into toys_rent.kategori_item(nama_item, nama_kategori) values(%s, %s)", (dict_req['nama'][0], kategori,))

        return redirect('item:show_items', updated = "new")

    else:
        form_tambah_item = ItemForm()
        return render(req, 'create.html', {"role" : req.session['role'], 'form_tambah_item' : form_tambah_item})

def delete_item(req):
    if 'role' not in req.session:
        return redirect("toysrent_auth:login")
    
    if req.session['role'] != "admin":
        return redirect("user:profile")

    if req.method == 'POST' and req.is_ajax():
        data = json.loads(req.body)

        with connection.cursor() as control:
            control.execute("delete from toys_rent.item where nama = %s", (data['item_name'],))
        
        return JsonResponse({'success': True})

def update_item(req, item_name):
    if 'role' not in req.session:
        return redirect("toysrent_auth:login")
    
    if req.session['role'] != "admin":
        return redirect("user:profile")
        
    if req.method == 'GET':
        # init_categories()
        item_data = get_item(item_name)
        item_categories = get_item_category(item_name)

        item_data[0]['kategoriT'] = []

        for item_category in item_categories:
            item_data[0]['kategoriT'].append(item_category['nama_kategori'])

        form_update_item = ItemForm(initial =
            {
                'nama' : item_name,
                'deskripsi' : item_data[0]['deskripsi'],
                'usia_awal' : item_data[0]['usia_dari'],
                'usia_akhir' : item_data[0]['usia_sampai'],
                'bahan' : item_data[0]['bahan'],
                'kategori': item_data[0]['kategoriT']
            }
        )

        return render(req, 'update.html', {"role" : req.session['role'], 'form_update_item' : form_update_item})
    else:
        dict_req = dict(req.POST)
        item_categories = get_item_category(item_name)

        with connection.cursor() as control:
            control.execute("update toys_rent.item set nama=%s, deskripsi=%s, usia_dari=%s, usia_sampai=%s, bahan=%s where nama=%s",
                            (item_name, req.POST['deskripsi'], int(req.POST['usia_awal']), int(req.POST['usia_akhir']), req.POST['bahan'], item_name,))
        
            found = False
            for kategori in dict_req['kategori']:
                found = False
                for item_category in item_categories:
                    if kategori in item_category.values():
                        found = True
                if not found:
                    control.execute("insert into toys_rent.kategori_item(nama_item, nama_kategori) values(%s, %s)", (item_name, kategori,))
            
            for item_category in item_categories:
                if item_category['nama_kategori'] not in dict_req['kategori']:
                    control.execute("delete from toys_rent.kategori_item where nama_item=%s and nama_kategori=%s", (item_name, item_category['nama_kategori'],))
        
        return redirect('item:show_items', updated = "new")
                    

            