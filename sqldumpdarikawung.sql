--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.22
-- Dumped by pg_dump version 11.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: toys_rent; Type: SCHEMA; Schema: -; Owner: db2018039
--

CREATE SCHEMA toys_rent;


ALTER SCHEMA toys_rent OWNER TO db2018039;

--
-- Name: add_points_barang(); Type: FUNCTION; Schema: toys_rent; Owner: db2018039
--

CREATE FUNCTION toys_rent.add_points_barang() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
update anggota set poin = poin + 100
where no_ktp = new.no_ktp_penyewa;
return new;
end;
$$;


ALTER FUNCTION toys_rent.add_points_barang() OWNER TO db2018039;

--
-- Name: add_points_pemesanan(); Type: FUNCTION; Schema: toys_rent; Owner: db2018039
--

CREATE FUNCTION toys_rent.add_points_pemesanan() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
update anggota set poin = poin + new.kuantitas_barang * 100
where no_ktp = new.no_ktp_pemesan;
return new;
end;
$$;


ALTER FUNCTION toys_rent.add_points_pemesanan() OWNER TO db2018039;

--
-- Name: cek_level_tambah_poin(); Type: FUNCTION; Schema: toys_rent; Owner: db2018039
--

CREATE FUNCTION toys_rent.cek_level_tambah_poin() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN



IF (TG_OP = 'UPDATE') THEN
UPDATE ANGGOTA SET level = get_nama_level(NEW.poin) WHERE no_ktp = NEW.no_ktp;
RETURN NEW;
END IF;

END;
$$;


ALTER FUNCTION toys_rent.cek_level_tambah_poin() OWNER TO db2018039;

--
-- Name: cek_pemesanan_semua_barang(); Type: FUNCTION; Schema: toys_rent; Owner: db2018039
--

CREATE FUNCTION toys_rent.cek_pemesanan_semua_barang() RETURNS void
    LANGUAGE plpgsql
    AS $$
    BEGIN
        UPDATE BARANG BR
        SET BR.kondisi = 'sedang disewa anggota'
        WHERE BR.id_barang IN
            (
            SELECT DISTINCT BP.id_barang
            FROM PEMESANAN P, BARANG_PESANAN BP
            WHERE P.id_pemesanan = BP.id_pemesanan
            );
    END;
$$;


ALTER FUNCTION toys_rent.cek_pemesanan_semua_barang() OWNER TO db2018039;

--
-- Name: get_nama_level(real); Type: FUNCTION; Schema: toys_rent; Owner: db2018039
--

CREATE FUNCTION toys_rent.get_nama_level(poin real) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
nama_level_baru VARCHAR(20);

BEGIN

SELECT nama_level FROM level_keanggotaan WHERE minimum_poin <= poin ORDER BY minimum_poin DESC LIMIT 1 INTO nama_level_baru;

RETURN nama_level_baru;
END;
$$;


ALTER FUNCTION toys_rent.get_nama_level(poin real) OWNER TO db2018039;

--
-- Name: ubah_data_level(); Type: FUNCTION; Schema: toys_rent; Owner: db2018039
--

CREATE FUNCTION toys_rent.ubah_data_level() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
temp_row RECORD;

BEGIN

fOR temp_row IN
SELECT * FROM ANGGOTA
LOOP

UPDATE ANGGOTA SET level = get_nama_level(temp_row.poin) WHERE no_ktp = temp_row.no_ktp;

END LOOP;

IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE' )
THEN
RETURN NEW;
ELSIF (TG_OP = 'DELETE' )
THEN
RETURN OLD;
END IF;

END;
$$;


ALTER FUNCTION toys_rent.ubah_data_level() OWNER TO db2018039;

--
-- Name: update_harga_sewa(); Type: FUNCTION; Schema: toys_rent; Owner: db2018039
--

CREATE FUNCTION toys_rent.update_harga_sewa() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    DECLARE
        temp_level VARCHAR;
        temp_item INTEGER;
        temp_harga INTEGER;
        id_p VARCHAR := NEW.Id_pemesanan;
    BEGIN
        IF  (TG_OP = 'INSERT') THEN
            SELECT id_barang INTO temp_item FROM BARANG_PESANAN WHERE Id_pemesanan=id_p;
            SELECT Level INTO temp_level FROM ANGGOTA A JOIN PEMESANAN P ON A.No_ktp_pemesan = P.No_ktp WHERE P.Id_pemesanan=id_p;
            SELECT Harga_sewa INTO temp_harga FROM INFO_BARANG_LEVEL WHERE Nama_level=temp_level AND Id_barang=temp_item;
        
            UPDATE ONLY PEMESANAN P
                SET Harga_sewa=temp_harga
                WHERE P.Id_pemesanan=id_p;
        END IF;
        RETURN NEW;
    END;
$$;


ALTER FUNCTION toys_rent.update_harga_sewa() OWNER TO db2018039;

--
-- Name: update_harga_sewa(character varying); Type: FUNCTION; Schema: toys_rent; Owner: db2018039
--

CREATE FUNCTION toys_rent.update_harga_sewa(id_pemesanan character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
    DECLARE
        temp_level VARCHAR;
        temp_item INTEGER;
        temp_harga INTEGER;
    BEGIN
        SELECT id_barang INTO temp_item FROM BARANG_PESANAN WHERE Id_pemesanan=id_pemesanan;
        SELECT Level INTO temp_level FROM ANGGOTA A JOIN PEMESANAN P ON A.No_ktp_pemesan = P.No_ktp WHERE P.Id_pemesanan=id_pemesanan;
        SELECT Harga_sewa INTO temp_harga FROM INFO_BARANG_LEVEL WHERE Nama_level=temp_level AND Id_barang=temp_item;
    
        UPDATE ONLY PEMESANAN P
            SET Harga_sewa=temp_harga
            WHERE P.Id_pemesanan=id_pemesanan;
    END;
$$;


ALTER FUNCTION toys_rent.update_harga_sewa(id_pemesanan character varying) OWNER TO db2018039;

--
-- Name: update_kondisi(); Type: FUNCTION; Schema: toys_rent; Owner: db2018039
--

CREATE FUNCTION toys_rent.update_kondisi() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        PERFORM cek_pemesanan_semua_barang();
        RETURN NEW;
    END;
$$;


ALTER FUNCTION toys_rent.update_kondisi() OWNER TO db2018039;

--
-- Name: update_ongkos(); Type: FUNCTION; Schema: toys_rent; Owner: db2018039
--

CREATE FUNCTION toys_rent.update_ongkos() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    DECLARE
        temp_ongkos INTEGER;
        id_pemesanan VARCHAR := NEW.Id_pemesanan;
    BEGIN
    SELECT Ongkos INTO temp_ongkos FROM PENGIRIMAN WHERE Id_pemesanan=id_pemesanan;

    IF (TG_OP = 'INSERT') THEN

        UPDATE ONLY PEMESANAN P
            SET P.Ongkos = P.Ongkos + NEW.Ongkos
            WHERE Id_pemesanan=id_pemesanan;
    END IF;
    END;
$$;


ALTER FUNCTION toys_rent.update_ongkos() OWNER TO db2018039;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin; Type: TABLE; Schema: toys_rent; Owner: db2018039
--

CREATE TABLE toys_rent.admin (
    no_ktp character varying(20) NOT NULL
);


ALTER TABLE toys_rent.admin OWNER TO db2018039;

--
-- Name: alamat; Type: TABLE; Schema: toys_rent; Owner: db2018039
--

CREATE TABLE toys_rent.alamat (
    no_ktp_anggota character varying(20) NOT NULL,
    nama character varying(255) NOT NULL,
    jalan character varying(255) NOT NULL,
    nomor integer NOT NULL,
    kota character varying(255) NOT NULL,
    kodepos character varying(10) NOT NULL
);


ALTER TABLE toys_rent.alamat OWNER TO db2018039;

--
-- Name: anggota; Type: TABLE; Schema: toys_rent; Owner: db2018039
--

CREATE TABLE toys_rent.anggota (
    no_ktp character varying(20) NOT NULL,
    poin real NOT NULL,
    level character varying(20)
);


ALTER TABLE toys_rent.anggota OWNER TO db2018039;

--
-- Name: barang; Type: TABLE; Schema: toys_rent; Owner: db2018039
--

CREATE TABLE toys_rent.barang (
    id_barang character varying(10) NOT NULL,
    nama_item character varying(255) NOT NULL,
    warna character varying(50),
    url_foto text,
    kondisi text NOT NULL,
    lama_penggunaan integer,
    no_ktp_penyewa character varying(20) NOT NULL
);


ALTER TABLE toys_rent.barang OWNER TO db2018039;

--
-- Name: barang_dikembalikan; Type: TABLE; Schema: toys_rent; Owner: db2018039
--

CREATE TABLE toys_rent.barang_dikembalikan (
    no_resi character varying(10) NOT NULL,
    id_barang character varying(10),
    no_urut character varying(10) NOT NULL
);


ALTER TABLE toys_rent.barang_dikembalikan OWNER TO db2018039;

--
-- Name: barang_dikirim; Type: TABLE; Schema: toys_rent; Owner: db2018039
--

CREATE TABLE toys_rent.barang_dikirim (
    no_resi character varying(10) NOT NULL,
    id_barang character varying(10),
    tanggal_review date NOT NULL,
    review text NOT NULL,
    no_urut character varying(10) NOT NULL
);


ALTER TABLE toys_rent.barang_dikirim OWNER TO db2018039;

--
-- Name: barang_pesanan; Type: TABLE; Schema: toys_rent; Owner: db2018039
--

CREATE TABLE toys_rent.barang_pesanan (
    id_pemesanan character varying(10) NOT NULL,
    no_urut character varying(10) NOT NULL,
    id_barang character varying(10),
    tanggal_sewa date NOT NULL,
    lama_sewa integer NOT NULL,
    tanggal_kembali date,
    status character varying(50) NOT NULL
);


ALTER TABLE toys_rent.barang_pesanan OWNER TO db2018039;

--
-- Name: chat; Type: TABLE; Schema: toys_rent; Owner: db2018039
--

CREATE TABLE toys_rent.chat (
    id character varying(15) NOT NULL,
    pesan text NOT NULL,
    datetime timestamp without time zone NOT NULL,
    no_ktp_anggota character varying(20) NOT NULL,
    no_ktp_admin character varying(20) NOT NULL
);


ALTER TABLE toys_rent.chat OWNER TO db2018039;

--
-- Name: info_barang_level; Type: TABLE; Schema: toys_rent; Owner: db2018039
--

CREATE TABLE toys_rent.info_barang_level (
    id_barang character varying(10) NOT NULL,
    nama_level character varying(20) NOT NULL,
    harga_sewa real NOT NULL,
    porsi_royalti real NOT NULL
);


ALTER TABLE toys_rent.info_barang_level OWNER TO db2018039;

--
-- Name: item; Type: TABLE; Schema: toys_rent; Owner: db2018039
--

CREATE TABLE toys_rent.item (
    nama character varying(255) NOT NULL,
    deskripsi text,
    usia_dari integer NOT NULL,
    usia_sampai integer NOT NULL,
    bahan text
);


ALTER TABLE toys_rent.item OWNER TO db2018039;

--
-- Name: kategori; Type: TABLE; Schema: toys_rent; Owner: db2018039
--

CREATE TABLE toys_rent.kategori (
    nama character varying(255) NOT NULL,
    level integer NOT NULL,
    sub_dari character varying(255)
);


ALTER TABLE toys_rent.kategori OWNER TO db2018039;

--
-- Name: kategori_item; Type: TABLE; Schema: toys_rent; Owner: db2018039
--

CREATE TABLE toys_rent.kategori_item (
    nama_item character varying(255) NOT NULL,
    nama_kategori character varying(255) NOT NULL
);


ALTER TABLE toys_rent.kategori_item OWNER TO db2018039;

--
-- Name: level_keanggotaan; Type: TABLE; Schema: toys_rent; Owner: db2018039
--

CREATE TABLE toys_rent.level_keanggotaan (
    nama_level character varying(20) NOT NULL,
    minimum_poin real NOT NULL,
    deskripsi text
);


ALTER TABLE toys_rent.level_keanggotaan OWNER TO db2018039;

--
-- Name: pemesanan; Type: TABLE; Schema: toys_rent; Owner: db2018039
--

CREATE TABLE toys_rent.pemesanan (
    id_pemesanan character varying(10) NOT NULL,
    datetime_pesanan timestamp without time zone NOT NULL,
    kuantitas_barang integer NOT NULL,
    harga_sewa real,
    ongkos real,
    no_ktp_pemesan character varying(20) NOT NULL,
    status character varying(50)
);


ALTER TABLE toys_rent.pemesanan OWNER TO db2018039;

--
-- Name: pengembalian; Type: TABLE; Schema: toys_rent; Owner: db2018039
--

CREATE TABLE toys_rent.pengembalian (
    no_resi character varying(10) NOT NULL,
    id_pemesanan character varying(10),
    metode text NOT NULL,
    ongkos real NOT NULL,
    tanggal date NOT NULL,
    no_ktp_anggota character varying(20),
    nama_alamat_anggota character varying(255)
);


ALTER TABLE toys_rent.pengembalian OWNER TO db2018039;

--
-- Name: pengguna; Type: TABLE; Schema: toys_rent; Owner: db2018039
--

CREATE TABLE toys_rent.pengguna (
    no_ktp character varying(20) NOT NULL,
    nama_lengkap character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    tanggal_lahir date,
    no_telp character varying(20)
);


ALTER TABLE toys_rent.pengguna OWNER TO db2018039;

--
-- Name: pengiriman; Type: TABLE; Schema: toys_rent; Owner: db2018039
--

CREATE TABLE toys_rent.pengiriman (
    no_resi character varying(10) NOT NULL,
    id_pemesanan character varying(10),
    metode text NOT NULL,
    ongkos real NOT NULL,
    tanggal date NOT NULL,
    no_ktp_anggota character varying(20),
    nama_alamat_anggota character varying(255)
);


ALTER TABLE toys_rent.pengiriman OWNER TO db2018039;

--
-- Name: status; Type: TABLE; Schema: toys_rent; Owner: db2018039
--

CREATE TABLE toys_rent.status (
    nama character varying(50) NOT NULL,
    deskripsi text
);


ALTER TABLE toys_rent.status OWNER TO db2018039;

--
-- Data for Name: admin; Type: TABLE DATA; Schema: toys_rent; Owner: db2018039
--

COPY toys_rent.admin (no_ktp) FROM stdin;
769-54-4263
244-54-0862
169-27-1161
415-88-6845
219-07-7284
699-28-7792
434-83-5211
840-98-9871
210-67-5616
355-88-6116
\.


--
-- Data for Name: alamat; Type: TABLE DATA; Schema: toys_rent; Owner: db2018039
--

COPY toys_rent.alamat (no_ktp_anggota, nama, jalan, nomor, kota, kodepos) FROM stdin;
769-54-4263	Simeon Well	aliquet	55	nec,	9790
244-54-0862	Ruggiero Schultze	in,	98	magna	39404
415-88-6845	Derrik Smullen	tristique	53	enim	50627
219-07-7284	Ellerey De Vuyst	sem,	98	Sed	53358
699-28-7792	Clo Balducci	ornare,	110	erat	10826
434-83-5211	Allan McClounan	nisi	95	Donec	49035
169-27-1161	Marita Menichini	Nunc	82	quam.	21801
210-67-5616	Drake Morland	odio	79	commodo	8851
355-88-6116	Misty Penrith	commodo	96	neque.	66602
830-50-3193	Aloin Copcutt	tincidunt	25	purus	94636
520-55-6251	Barton Sprigin	a,	193	ut	18397
866-89-7074	Zora Rentilll	vel,	10	eleifend	40395
688-36-0929	Marmaduke McKleod	Praesent	199	vitae,	82650
330-43-7650	Kandy Kroger	Pellentesque	6	sed	92655
481-99-4642	Ulrich Nern	Suspendisse	41	placerat,	38067
704-02-7072	Laurie Kellie	accumsan	179	sed	69997
818-63-8795	Delmore Royal	scelerisque	176	non,	53602
525-13-4486	Natalee Grassin	mus.	134	accumsan	63721
356-50-2151	Nadine Eatherton	Suspendisse	62	pharetra.	93590
808-77-0426	Creighton Beushaw	cursus	119	Curabitur	78051
428-77-4060	Cecily Rubinow	Etiam	38	ut	78823
109-67-2867	Jane Candish	Nulla	32	Quisque	91461
644-81-5787	Daren Raraty	dignissim	26	laoreet,	46331
115-06-0419	Shari Hannum	vulputate,	52	erat	58803
236-16-1747	Theresina Fitter	diam.	120	risus.	50009
647-84-9703	Janaye Goodisson	Sed	39	vel	5299
300-71-7963	Brady Sarfatti	facilisis,	26	enim	95768
672-01-3901	Iago Chasmer	dictum	171	Proin	36832
823-03-5197	Tibold Chamney	Duis	8	eget,	97295
187-32-5699	Nonna Randleson	metus	109	id,	12481
456-74-5371	Selina Scotti	magna	143	mauris.	23594
443-25-5572	Leo Lay	sem.	128	facilisis	59543
495-74-7629	Jasmin Danieli	sagittis.	69	dictum	89805
671-42-8852	Aeriel Griniov	nec	84	at	69744
284-14-9248	Isa Smallacombe	natoque	128	tristique	20960
821-83-7217	Eugine Welsh	Donec	46	aliquam	41881
359-09-0091	Yolane Tows	a	33	sollicitudin	47166
518-75-8714	George Marcq	massa.	108	Integer	42102
811-34-4269	Bethanne Cutridge	sit	149	consequat	99582
864-69-9469	Gregg Rummins	non,	157	vitae,	83944
457-51-9515	Blondelle Strephan	sit	112	vel	29969
100-26-1701	Sampson Neubigging	nec,	84	auctor,	69927
704-56-0789	Bonnie Petricek	ultricies	199	mattis.	91804
769-01-4151	Velma Duligal	quam	32	ipsum	96078
649-13-3997	Ophelie Winsborrow	sagittis	173	turpis.	39653
279-68-6913	Lovell Semorad	lacus.	158	rutrum.	51276
209-72-6561	Bevan Harrold	diam.	151	non	71024
865-61-1176	Jules Yakovich	placerat,	25	sollicitudin	52109
880-19-4901	Michale Kirke	in	191	diam	98815
679-87-9655	Nicki Palethorpe	non,	172	nonummy	4799
874-87-7806	Shalne McCorry	porttitor	41	ullamcorper,	66252
762-51-8697	Lowrance Bausmann	justo.	40	gravida	10037
500-28-0196	Gretel Yushkov	ligula.	73	Donec	49945
858-86-7166	Briana Risely	felis.	195	sagittis.	38346
197-59-5675	Hildagarde Burbage	aliquet	77	id	88327
892-31-0530	Avis Harbron	sed,	39	vel,	81207
854-96-8799	Otes Brien	sem	51	malesuada	3522
566-74-2960	Dolf Corradetti	blandit	164	imperdiet	20837
571-17-7573	Stephine Yewdale	nibh.	135	montes,	98062
119-70-1944	Cass Langridge	feugiat	187	Donec	79887
268-63-7402	Les Schutte	ac	14	enim.	58958
120-80-8048	Roger Ullyott	Aenean	200	ac	39280
665-22-0469	Timmy Iacovuzzi	est	131	vel,	47025
787-55-8524	Charissa Varley	eu	191	fringilla.	15073
371-02-2990	Aubrie McPeice	dapibus	141	Duis	3865
405-72-2139	Lenora Ranger	tellus.	32	Donec	49929
787-16-0278	Lil Cayle	malesuada	19	consequat,	86633
198-72-3705	Eugenia Masdin	nisi	140	Duis	7640
775-59-9748	Reta Willcot	commodo	98	quam.	41205
565-09-6695	Briny Battista	tempus,	186	est	7442
838-20-0423	Cele Gilvary	et	144	pharetra	25339
511-41-6666	Maxine Rate	a	152	arcu	42005
835-85-7566	Silvester Beynon	bibendum	174	at,	25600
171-49-4056	Anjela Ibberson	Donec	21	eget	86719
840-98-9871	Garrett Delagnes	ut	74	ligula.	5568
769-56-7751	Artair Eberle	urna	199	vehicula.	2392
839-04-6798	Rubie Bray	et,	167	luctus	44810
564-76-8185	Lynsey Jakubovsky	ipsum	71	a,	72407
320-64-4874	Rolland Essex	libero	135	pede.	18697
803-37-2157	Caril Karslake	Nullam	197	Ut	4719
486-08-5371	Aurelie Murrthum	faucibus.	15	aliquet	20873
803-96-7763	Hadleigh Dany	rutrum	57	ridiculus	96310
557-91-8304	Jacynth Mathivet	risus	170	et	90210
331-65-3270	Nikos Stuttard	fringilla	28	fringilla	68065
586-61-3217	Pete Nial	venenatis	26	tellus	10154
221-11-0887	Gabey Harburtson	mollis.	136	Donec	22480
628-24-4154	Amitie Heinish	a	161	Quisque	89402
824-26-5666	Sukey Toland	nulla	171	quam	70521
829-09-8519	Karly Twitchings	Vestibulum	152	neque.	38628
546-70-8324	Sollie Hutchings	scelerisque	95	sagittis	30854
481-50-3155	Ban Meale	vel	38	ut	86169
790-72-3809	Joby Colmer	Integer	5	venenatis	64253
553-68-6779	Lanny Grouen	faucibus	133	sed	21739
118-16-1795	Wynny Spatig	et,	12	magna.	70941
200-89-0821	Veronika Newlin	turpis	43	vitae	10991
495-72-7317	Pepillo MacShirrie	vitae	29	adipiscing.	32029
140-84-7136	Avrit Hinckes	rutrum	117	Phasellus	75941
771-55-2007	Mordy Scutter	gravida.	125	sit	95281
526-33-5938	Nap Kuhnt	sollicitudin	6	pede	16050
628-57-6193	Jaine Cusick	ullamcorper.	119	Vestibulum	40487
\.


--
-- Data for Name: anggota; Type: TABLE DATA; Schema: toys_rent; Owner: db2018039
--

COPY toys_rent.anggota (no_ktp, poin, level) FROM stdin;
839-04-6798	41	BRONZE
564-76-8185	161	SILVER
840-98-9871	22	BRONZE
553-68-6779	33	BRONZE
571-17-7573	162	SILVER
119-70-1944	132	SILVER
118-16-1795	44	BRONZE
120-80-8048	103	SILVER
200-89-0821	55	SILVER
787-55-8524	54	SILVER
371-02-2990	90	SILVER
405-72-2139	32	BRONZE
787-16-0278	95	SILVER
198-72-3705	45	BRONZE
495-72-7317	66	SILVER
565-09-6695	138	SILVER
838-20-0423	103	SILVER
511-41-6666	190	GOLD
835-85-7566	190	GOLD
171-49-4056	180	GOLD
769-56-7751	131	SILVER
140-84-7136	88	SILVER
771-55-2007	77	SILVER
320-64-4874	192	GOLD
803-37-2157	59	SILVER
769-54-4263	13	\N
244-54-0862	180	GOLD
415-88-6845	157	SILVER
219-07-7284	60	SILVER
699-28-7792	197	GOLD
434-83-5211	53	SILVER
210-67-5616	165	GOLD
355-88-6116	74	SILVER
830-50-3193	99	SILVER
520-55-6251	97	SILVER
866-89-7074	189	GOLD
688-36-0929	94	SILVER
330-43-7650	32	BRONZE
481-99-4642	21	BRONZE
704-02-7072	183	GOLD
818-63-8795	1	\N
525-13-4486	116	SILVER
356-50-2151	140	SILVER
808-77-0426	38	BRONZE
428-77-4060	113	SILVER
109-67-2867	113	SILVER
644-81-5787	91	SILVER
115-06-0419	75	SILVER
236-16-1747	153	SILVER
647-84-9703	54	SILVER
300-71-7963	68	SILVER
672-01-3901	181	GOLD
823-03-5197	183	GOLD
187-32-5699	86	SILVER
456-74-5371	110	SILVER
443-25-5572	61	SILVER
495-74-7629	77	SILVER
671-42-8852	180	GOLD
284-14-9248	106	SILVER
821-83-7217	63	SILVER
359-09-0091	100	SILVER
518-75-8714	170	GOLD
811-34-4269	58	SILVER
864-69-9469	126	SILVER
457-51-9515	91	SILVER
100-26-1701	138	SILVER
704-56-0789	105	SILVER
769-01-4151	41	BRONZE
649-13-3997	60	SILVER
279-68-6913	83	SILVER
209-72-6561	92	SILVER
865-61-1176	100	SILVER
880-19-4901	70	SILVER
679-87-9655	156	SILVER
874-87-7806	100	SILVER
762-51-8697	114	SILVER
500-28-0196	161	SILVER
858-86-7166	111	SILVER
197-59-5675	68	SILVER
892-31-0530	29	BRONZE
854-96-8799	125	SILVER
566-74-2960	176	GOLD
268-63-7402	120	SILVER
665-22-0469	6	\N
775-59-9748	51	SILVER
486-08-5371	138	SILVER
803-96-7763	81	SILVER
557-91-8304	99	SILVER
331-65-3270	72	SILVER
586-61-3217	33	BRONZE
221-11-0887	199	GOLD
628-24-4154	27	BRONZE
824-26-5666	96	SILVER
829-09-8519	135	SILVER
546-70-8324	123	SILVER
481-50-3155	165	GOLD
526-33-5938	99	SILVER
628-57-6193	111	SILVER
169-27-1161	123	SILVER
1234	5000	testingtiga
790-72-3809	101	SILVER
\.


--
-- Data for Name: barang; Type: TABLE DATA; Schema: toys_rent; Owner: db2018039
--

COPY toys_rent.barang (id_barang, nama_item, warna, url_foto, kondisi, lama_penggunaan, no_ktp_penyewa) FROM stdin;
7134617969	cursus,	eget mollis	IXX94ZKV9HA	mi	29	300-71-7963
6111727084	arcu.	gravida sagittis. Duis	HME63JBF9FS	vitae velit egestas lacinia. Sed	53	405-72-2139
1181374463	mauris,	consequat	GSL82KKD0UU	elit. Curabitur sed tortor. Integer	55	649-13-3997
1464127971	erat	Aenean	SBO60EXJ6GK	Phasellus elit pede, malesuada	53	769-01-4151
8742528960	erat	porttitor vulputate,	JVP72QLA6VI	vitae	21	236-16-1747
9073852556	luctus	et magnis dis	TWG95IVN7MA	et ipsum	16	187-32-5699
2861235396	malesuada	metus.	OPK84EIO4CA	sit amet nulla. Donec non	8	803-37-2157
7770956026	a	ante blandit viverra.	LSO33LBM3NE	vitae, sodales at, velit.	31	443-25-5572
2674108313	erat	Aliquam ultrices	ZDI00ILG7YC	risus a	11	553-68-6779
4512712514	ssss	eros. Nam consequat	SPT87QPK5KV	amet risus. Donec	7	671-42-8852
518613938	ac	massa. Mauris	JOY84IBY4FR	semper erat, in	15	244-54-0862
8231037796	ac	dui.	JGN71RNF4TS	laoreet posuere, enim	53	210-67-5616
9698400750	arcu.	orci.	RTX23KUY9LQ	lorem	11	197-59-5675
8964559265	ridiculus	pede et	MTQ03CFA5FA	velit egestas	55	456-74-5371
4684114681	fermentum	hendrerit	MBQ40WOM0PA	egestas a, dui. Cras	\N	209-72-6561
1892366027	luctus	aliquet odio.	TWV00EKM2OX	sagittis. Duis gravida. Praesent	\N	330-43-7650
8997806956	ssss	libero dui nec	WIN36FAC3JY	ligula.	\N	415-88-6845
5245640631	Mauris	at, nisi.	VBI56KXV6WF	massa rutrum magna. Cras	\N	279-68-6913
3130407525	eu	consectetuer adipiscing elit.	HCT44KWJ5KY	Pellentesque habitant morbi	\N	456-74-5371
8213476364	a,	montes, nascetur ridiculus	ZOG14HDP4SF	ullamcorper	\N	415-88-6845
4542464058	cursus.	vitae risus. Duis	LTL88MLP1CJ	Donec tempus, lorem fringilla	\N	762-51-8697
6066711389	fermentum	odio.	ONB73NIE9HP	quam a felis	\N	120-80-8048
2174254579	malesuada	ut, molestie in,	GBX13EMZ6OS	ullamcorper. Duis	\N	769-56-7751
4809172419	ac	diam	YCG14KJZ2XW	fames	\N	818-63-8795
8731118598	a,	malesuada	HUN66ZJR1LK	fringilla ornare placerat, orci	\N	839-04-6798
6018723876	consectetuer,	vulputate, posuere vulputate,	MYZ50DSD7YC	aliquet nec, imperdiet	\N	787-55-8524
7079656914	a	nonummy ac,	IMB58QYD1UP	sed tortor. Integer	\N	628-24-4154
6301186623	a,	semper	CSF63OAR9VM	lorem lorem, luctus ut, pellentesque	\N	874-87-7806
7567807428	enim.	molestie	CEL43HAO1BJ	lacus. Cras	\N	244-54-0862
6642946488	fermentum	Quisque imperdiet,	JDV79LVY8RB	nibh dolor, nonummy ac, feugiat	\N	520-55-6251
2772047441	luctus	amet,	KFE26FVU7VV	tellus. Suspendisse sed dolor.	\N	120-80-8048
7126472604	et	scelerisque	JCP41ZHM8UX	penatibus et magnis dis parturient	\N	649-13-3997
4490966027	luctus	montes, nascetur ridiculus	SZZ08GLS7RN	per inceptos hymenaeos.	\N	787-55-8524
4952042477	ac	purus.	KHD01QJZ8KM	lectus,	\N	839-04-6798
7314675138	consectetuer,	et,	NUF67QFD7NE	eu neque pellentesque massa	\N	803-37-2157
4245368459	purus	orci,	HOR67DYN7QD	Phasellus vitae	\N	787-16-0278
1914046727	ridiculus	lacus. Ut	CKL99OAV0LG	sociosqu	\N	359-09-0091
2235269854	arcu.	placerat velit.	LPV86OJW1WO	ornare	\N	808-77-0426
4807782992	velit.	nec metus facilisis	DTE60RYF6HS	tristique ac, eleifend	\N	330-43-7650
4473982818	ac	mollis	NFA72JWB6NO	dui,	\N	330-43-7650
6856134946	purus	interdum	CFZ80MPF4IX	litora torquent	\N	209-72-6561
1490312514	et	libero.	CRT64KKX7WW	senectus	\N	518-75-8714
9202009527	Phasellus	nascetur ridiculus	YYQ65RDJ1GX	accumsan	\N	320-64-4874
9074625352	Phasellus	aliquet, metus	ZNA00TDN3QE	aliquet	\N	840-98-9871
2439945150	enim.	convallis convallis	NBL46NWD0TM	Aenean	\N	525-13-4486
1213378830	enim.	lorem eu	PIL12PNB9OT	ac risus. Morbi	\N	415-88-6845
7066353248	purus	nibh.	TGM02PKI6RS	Sed nunc	\N	518-75-8714
9834033716	cursus,	id risus	DBX74KGK8AW	Integer vitae	\N	866-89-7074
3143442621	Mauris	ipsum. Suspendisse non	BWW10DTH7OF	arcu eu odio tristique	\N	198-72-3705
2418207600	cursus.	auctor, velit	NZH38DWI7LT	eros non enim commodo hendrerit.	\N	187-32-5699
1969545018	Mauris	diam at pretium	IQD98QHT4YP	velit eget laoreet posuere,	\N	811-34-4269
255967379	mi	fringilla mi lacinia	NLX92RLH2OX	suscipit nonummy. Fusce fermentum	\N	839-04-6798
1630161595	velit.	at arcu. Vestibulum	PNC48UDI9XI	eget metus. In	\N	100-26-1701
8997075839	volutpat.	massa.	TYL21QSA9NB	lacinia	\N	854-96-8799
7717184818	purus.	Donec egestas. Aliquam	CHZ10JRF4KM	montes, nascetur	\N	300-71-7963
5283217490	mauris,	aliquet, sem	MSO50IFZ4ZN	Sed eu nibh vulputate	\N	892-31-0530
7877690412	fermentum	erat, eget tincidunt	CYO62XEF5BX	Cum	\N	525-13-4486
4078492082	luctus	a sollicitudin	ZJT72OLN1OL	Suspendisse aliquet molestie tellus.	\N	866-89-7074
9182954088	purus	nostra,	XLT57XSZ4BZ	metus.	\N	518-75-8714
572786714	ac	nibh.	QRR75JJL3KK	massa. Suspendisse	\N	672-01-3901
5941951736	et	risus. Duis	ALI46HYT4VC	Cras	\N	824-26-5666
2086614043	erat	non,	RDZ55PDZ5RS	bibendum ullamcorper.	\N	320-64-4874
9968669854	volutpat.	metus sit	ETB15WSP5ZY	massa. Mauris vestibulum, neque	\N	762-51-8697
7753053445	cursus.	risus	DEJ36NLE0XH	Sed id risus	\N	566-74-2960
7358907856	arcu.	dui nec	YMZ09GNO5QL	Nunc ullamcorper, velit	\N	518-75-8714
7276438752	enim.	nulla.	DEF71VJR4OM	diam at pretium aliquet, metus	\N	892-31-0530
7480190030	mi	velit	TNK36IRZ9YJ	Duis dignissim	\N	775-59-9748
4323377206	mauris,	Proin	TIL75EJX8GM	aliquet	\N	787-16-0278
2424345958	erat	augue. Sed molestie.	VLN52ACZ8GJ	adipiscing	\N	866-89-7074
4465084451	ac	massa. Suspendisse	GHD53ZVF5DJ	magnis dis parturient	\N	209-72-6561
3243846560	enim.	aliquet, sem ut	REF98CYB9VF	ac, eleifend vitae, erat.	\N	553-68-6779
7628006728	erat	Quisque	YEW94CFB1PZ	lectus pede et	\N	647-84-9703
2974716868	luctus	vel turpis. Aliquam	KIQ37QBL7XX	libero. Morbi accumsan laoreet ipsum.	\N	284-14-9248
3276822725	et	eu neque pellentesque	TEV78CWL3HQ	euismod in, dolor. Fusce feugiat.	\N	824-26-5666
2575675219	erat	imperdiet nec,	JCI24FBI4XB	semper. Nam	\N	210-67-5616
9731179877	cursus,	sagittis lobortis	LMB78HYL8RP	luctus ut, pellentesque	\N	644-81-5787
8653893676	eu	porttitor	XGF04TUF4KF	ante dictum	\N	481-50-3155
9606560746	commodo	nascetur	FFZ36WHD6TS	mollis.	\N	356-50-2151
8444396379	et	nisl sem, consequat	QEC02GPH7WY	amet, consectetuer adipiscing	\N	672-01-3901
5912404602	eu	Nam	FBN45AZD6SH	mauris ut mi.	\N	495-72-7317
4891587872	ac	euismod in, dolor.	AUW71WWA5LQ	vulputate, lacus. Cras	\N	838-20-0423
6844429861	commodo	ante	FKG32DLL6LV	vulputate	\N	115-06-0419
3901826434	ridiculus	magna. Nam	SFN24IRL6WN	at augue id	\N	526-33-5938
4506486746	enim.	nibh dolor,	YHV82DFH1AK	a purus. Duis elementum,	\N	140-84-7136
1150732581	a,	dignissim lacus. Aliquam	VNL93YAS4JD	risus.	\N	359-09-0091
9100064463	mi	Donec sollicitudin	BFZ12YGX7BZ	at, nisi. Cum sociis natoque	\N	803-37-2157
3842633393	erat	porttitor eros nec	AMT29APQ3TT	Duis elementum,	\N	564-76-8185
5472116265	a,	vitae, posuere	WCQ97LHJ8OM	urna convallis erat,	\N	371-02-2990
9692759080	luctus	non sapien	IBK61TFJ5YM	Duis mi	\N	511-41-6666
7514327435	mi	augue ut lacus.	EDV15HXJ8WR	senectus et netus	\N	443-25-5572
8681406680	ridiculus	lectus	OWX08KJI3HV	ornare.	\N	405-72-2139
3833595683	ac	euismod ac, fermentum	XUB84DDJ5NV	ultrices posuere	\N	355-88-6116
6495828210	volutpat.	ultrices iaculis odio.	HFR73OEU4BX	ornare, lectus ante dictum	\N	565-09-6695
6671249147	commodo	sagittis semper. Nam	IDI18VUU9XF	amet, risus. Donec nibh	\N	330-43-7650
1625709193	a	Nunc ut	WGC21NWR1ZR	placerat, augue. Sed	\N	665-22-0469
2971322039	erat	Nunc lectus pede,	BOI05RDN5QS	convallis est, vitae	\N	236-16-1747
6671464639	cursus,	suscipit	JYH66XBM1UO	tempor, est ac mattis	\N	821-83-7217
9937647969	ridiculus	dolor egestas rhoncus.	HXS76BNQ4DR	amet nulla. Donec non	\N	284-14-9248
6691859401	purus.	mattis ornare,	WHD62TKH5FV	Nullam vitae	\N	221-11-0887
9843758873	eu	luctus,	FWW38SKZ6IU	Phasellus libero mauris,	\N	526-33-5938
1966919	erat	biru	www.google.com	baru	2	790-72-3809
\.


--
-- Data for Name: barang_dikembalikan; Type: TABLE DATA; Schema: toys_rent; Owner: db2018039
--

COPY toys_rent.barang_dikembalikan (no_resi, id_barang, no_urut) FROM stdin;
2982131805	7134617969	1
2177001304	6111727084	2
2510042057	1181374463	3
2664582539	1464127971	4
2635345491	8742528960	5
2268968038	9073852556	6
2038788439	2861235396	7
2843248633	7770956026	8
2661098325	2674108313	9
2041974134	4512712514	10
2386527035	518613938	11
2932342209	8231037796	12
2753411573	9698400750	13
2509636395	8964559265	14
2142870573	4684114681	15
2063896362	1892366027	16
2526272269	8997806956	17
2781987358	5245640631	18
2799899838	3130407525	19
2963444546	8213476364	20
2982131805	4542464058	21
2177001304	6066711389	22
2510042057	2174254579	23
2664582539	4809172419	24
2635345491	8731118598	25
2268968038	6018723876	26
2038788439	7079656914	27
2843248633	6301186623	28
2661098325	7567807428	29
2041974134	6642946488	30
2386527035	2772047441	31
2932342209	7126472604	32
2753411573	4490966027	33
2509636395	4952042477	34
2142870573	7314675138	35
2063896362	4245368459	36
2526272269	1914046727	37
2781987358	2235269854	38
2799899838	4807782992	39
2963444546	4473982818	40
\.


--
-- Data for Name: barang_dikirim; Type: TABLE DATA; Schema: toys_rent; Owner: db2018039
--

COPY toys_rent.barang_dikirim (no_resi, id_barang, tanggal_review, review, no_urut) FROM stdin;
2982131805	7134617969	2018-06-25	Insert of Infusion Dev into L Sternoclav Jt, Perc Approach	1
2177001304	6111727084	2018-12-16	Insert Intralum Dev in L Subclav Art, Perc Endo	2
2510042057	1181374463	2018-09-04	Dilate R Brach Art, Bifurc, w 2 Intralum Dev, Perc	3
2664582539	1464127971	2018-06-11	Stereotactic Other Photon Radiosurgery of Nasopharynx	4
2635345491	8742528960	2018-06-08	ROM & Jt Mobility Treatment of Neuro Body using Orthosis	5
2268968038	9073852556	2018-06-23	Excision of Left Upper Eyelid, Open Approach, Diagnostic	6
2038788439	2861235396	2018-07-24	Division of Left Upper Leg Tendon, Open Approach	7
2843248633	7770956026	2018-05-17	Restriction of Bladder Neck, Open Approach	8
2661098325	2674108313	2018-05-20	Drain of R Hand Subcu/Fascia with Drain Dev, Perc Approach	9
2041974134	4512712514	2018-05-24	Restrict of R Foot Art with Intralum Dev, Perc Endo Approach	10
2386527035	518613938	2018-12-13	Drainage of Left Ear Skin, External Approach, Diagnostic	11
2932342209	8231037796	2018-06-13	Removal of Other Device on Right Foot	12
2753411573	9698400750	2018-08-25	Excision of Left Spermatic Cord, Open Approach	13
2509636395	8964559265	2018-06-29	Supplement L Temporal Art with Autol Sub, Perc Approach	14
2142870573	4684114681	2018-07-13	Supplement R Ext Iliac Vein with Synth Sub, Open Approach	15
2063896362	1892366027	2018-09-18	Repair Right Abdomen Bursa and Ligament, Open Approach	16
2526272269	8997806956	2018-10-19	Restrict L Int Jugular Vein w Intralum Dev, Perc	17
2781987358	5245640631	2018-11-02	Revision of Drain Dev in R Extraoc Muscle, Open Approach	18
2799899838	3130407525	2018-08-18	Replacement of Hard Palate with Nonaut Sub, Extern Approach	19
2963444546	8213476364	2018-11-25	Division of Sacral Plexus, Percutaneous Approach	20
2982131805	4542464058	2018-11-29	Removal of Drainage Device from Nasal Bone, Open Approach	21
2177001304	6066711389	2018-11-02	Bypass Bi Ureter to Bladder with Nonaut Sub, Open Approach	22
2510042057	2174254579	2018-10-07	Bypass Right Femoral Artery to L Femor A, Open Approach	23
2664582539	4809172419	2018-05-29	Dilate R Verteb Art, Bifurc, w 2 Intralum Dev, Perc	24
2635345491	8731118598	2018-05-25	Extirpate of Matter from L Ext Auditory Canal, Perc Approach	25
2268968038	6018723876	2018-10-03	Restrict of R Thyroid Art with Intralum Dev, Open Approach	26
2038788439	7079656914	2018-10-28	Remove of Infusion Dev from Endocrine Gland, Extern Approach	27
2843248633	6301186623	2018-04-28	Revision of Other Device in Cranial Cav, Perc Endo Approach	28
2661098325	7567807428	2018-11-20	Drainage of Inferior Vena Cava, Perc Endo Approach, Diagn	29
2041974134	6642946488	2018-11-07	Drainage of L Innom Vein with Drain Dev, Open Approach	30
2386527035	2772047441	2018-06-15	Reposition Left Mandible, Percutaneous Endoscopic Approach	31
2932342209	7126472604	2018-06-17	Map Thalamus, Percutaneous Approach	32
2753411573	4490966027	2018-06-01	Supplement Larynx with Nonautologous Tissue Substitute, Endo	33
2509636395	4952042477	2018-11-21	Central Nervous System, Reposition	34
2142870573	7314675138	2018-06-10	Repair Cervicothoracic Vertebral Joint, Perc Approach	35
2063896362	4245368459	2018-09-08	Alteration of Lower Jaw with Synth Sub, Open Approach	36
2526272269	1914046727	2018-05-21	Revision of Infusion Device in Thymus, Percutaneous Approach	37
2781987358	2235269854	2018-08-16	Stereotactic Particulate Radiosurgery of Head and Neck	38
2799899838	4807782992	2018-06-10	Revision of Radioact Elem in R Pleural Cav, Open Approach	39
2963444546	4473982818	2018-05-26	Removal of Monitoring Device from Left Lung, Extern Approach	40
\.


--
-- Data for Name: barang_pesanan; Type: TABLE DATA; Schema: toys_rent; Owner: db2018039
--

COPY toys_rent.barang_pesanan (id_pemesanan, no_urut, id_barang, tanggal_sewa, lama_sewa, tanggal_kembali, status) FROM stdin;
1000000000	1	7134617969	2018-11-06	8	2018-03-12	Mauv
1000000001	2	6111727084	2018-07-06	5	2018-08-06	Blue
1000000002	3	1181374463	2018-10-26	11	2018-04-03	Yellow
1000000003	4	1464127971	2018-09-18	4	2019-02-07	Puce
1000000004	5	8742528960	2018-11-13	19	2018-12-28	Goldenrod
1000000005	6	9073852556	2018-10-19	11	2019-03-13	Aquamarine
1000000006	7	2861235396	2018-09-24	17	2018-04-27	Mauv
1000000007	8	7770956026	2018-04-22	1	2018-09-30	Blue
1000000008	9	2674108313	2018-08-03	5	2018-03-10	Yellow
1000000009	10	4512712514	2018-06-05	15	2018-10-08	Puce
1000000010	11	518613938	2018-05-28	20	2018-10-17	Goldenrod
1000000011	12	8231037796	2018-09-23	7	2019-01-27	Aquamarine
1000000012	13	9698400750	2018-05-22	19	2018-07-27	Mauv
1000000013	14	8964559265	2018-06-16	2	2018-06-08	Blue
1000000014	15	4684114681	2018-06-20	4	2018-03-06	Yellow
1000000015	16	1892366027	2018-10-09	12	2018-05-17	Puce
1000000016	17	8997806956	2018-08-31	16	2018-11-21	Goldenrod
1000000017	18	5245640631	2018-10-03	6	2018-07-04	Aquamarine
1000000018	19	3130407525	2018-12-29	6	2018-12-25	Mauv
1000000019	20	8213476364	2018-11-10	2	2019-02-26	Blue
1000000020	21	4542464058	2018-10-16	4	2018-05-01	Yellow
1000000021	22	6066711389	2018-04-27	19	2018-07-27	Puce
1000000022	23	2174254579	2018-06-06	4	2018-07-13	Goldenrod
1000000023	24	4809172419	2018-09-29	8	2018-01-11	Aquamarine
1000000024	25	8731118598	2018-06-05	4	2018-06-15	Mauv
1000000025	26	6018723876	2018-10-02	20	2018-06-28	Blue
1000000026	27	7079656914	2018-07-20	8	2018-03-18	Yellow
1000000027	28	6301186623	2018-06-22	9	2018-11-05	Puce
1000000028	29	7567807428	2018-07-10	12	2018-01-02	Goldenrod
1000000029	30	6642946488	2018-11-04	14	2019-03-05	Aquamarine
1000000030	31	2772047441	2018-09-15	8	2019-01-29	Mauv
1000000031	32	7126472604	2018-12-02	16	2018-07-08	Blue
1000000032	33	4490966027	2018-05-13	16	2018-03-26	Yellow
1000000033	34	4952042477	2018-08-23	13	2019-02-09	Puce
1000000034	35	7314675138	2018-12-18	1	2018-02-03	Goldenrod
1000000035	36	4245368459	2018-12-02	5	2019-01-27	Aquamarine
1000000036	37	1914046727	2018-11-04	6	2018-11-09	Mauv
1000000037	38	2235269854	2018-06-12	16	2019-01-31	Blue
1000000038	39	4807782992	2018-10-31	13	2018-01-01	Yellow
1000000039	40	4473982818	2018-09-19	13	2019-03-05	Puce
1000000040	41	6856134946	2018-11-28	13	2018-02-17	Goldenrod
1000000041	42	1490312514	2018-07-11	19	2018-06-06	Aquamarine
1000000042	43	9202009527	2018-05-19	15	2019-02-17	Mauv
1000000043	44	9074625352	2018-08-18	8	2018-12-23	Blue
1000000044	45	2439945150	2018-08-29	5	2018-09-02	Yellow
1000000045	46	1213378830	2018-06-21	1	2019-02-25	Puce
1000000046	47	7066353248	2018-10-28	16	2018-12-17	Goldenrod
1000000047	48	9834033716	2018-05-29	6	2018-03-02	Aquamarine
1000000048	49	3143442621	2018-08-31	14	2018-10-18	Mauv
1000000049	50	2418207600	2018-07-06	11	2018-12-28	Blue
1000000000	51	1969545018	2018-06-18	10	2018-10-17	Mauv
1000000001	52	255967379	2018-07-02	16	2019-02-18	Blue
1000000002	53	1630161595	2018-10-01	7	2018-10-09	Yellow
1000000003	54	8997075839	2018-11-02	7	2018-04-26	Puce
1000000004	55	7717184818	2018-07-29	2	2018-04-21	Goldenrod
1000000005	56	5283217490	2018-12-30	20	2019-03-04	Aquamarine
1000000006	57	7877690412	2018-08-26	15	2019-03-29	Mauv
1000000007	58	4078492082	2018-05-27	10	2018-09-03	Blue
1000000008	59	9182954088	2018-09-28	13	2018-08-26	Yellow
1000000009	60	572786714	2018-08-28	5	2018-05-12	Puce
1000000010	61	5941951736	2018-11-15	6	2018-04-08	Goldenrod
1000000011	62	2086614043	2018-09-05	18	2018-01-02	Aquamarine
1000000012	63	9968669854	2018-10-25	3	2019-02-15	Mauv
1000000013	64	7753053445	2018-10-13	16	2018-06-10	Blue
1000000014	65	7358907856	2018-07-28	5	2018-03-09	Yellow
1000000015	66	7276438752	2018-10-22	17	2019-03-11	Puce
1000000016	67	7480190030	2018-05-20	5	2019-04-07	Goldenrod
1000000017	68	4323377206	2018-12-27	18	2018-11-20	Aquamarine
1000000018	69	2424345958	2018-11-29	2	2018-12-02	Mauv
1000000019	70	4465084451	2018-05-30	18	2018-12-10	Blue
1000000020	71	3243846560	2018-12-05	18	2019-02-09	Yellow
1000000021	72	7628006728	2018-06-17	9	2018-04-02	Puce
1000000022	73	2974716868	2018-08-03	2	2018-09-14	Goldenrod
1000000023	74	3276822725	2018-11-09	12	2018-05-13	Aquamarine
1000000024	75	2575675219	2018-07-25	16	2018-07-02	Mauv
1000000025	76	9731179877	2018-06-25	6	2018-04-23	Blue
1000000026	77	8653893676	2018-08-05	3	2019-03-04	Yellow
1000000027	78	9606560746	2018-09-13	17	2018-02-14	Puce
1000000028	79	8444396379	2018-06-08	16	2019-01-11	Goldenrod
1000000029	80	5912404602	2018-06-19	6	2018-10-05	Aquamarine
1000000030	81	4891587872	2018-09-12	8	2018-05-29	Mauv
1000000031	82	6844429861	2018-10-13	13	2019-01-08	Blue
1000000032	83	3901826434	2018-08-05	11	2018-01-22	Yellow
1000000033	84	4506486746	2018-05-28	8	2018-08-05	Puce
1000000034	85	1150732581	2018-04-17	4	2019-02-04	Goldenrod
1000000035	86	9100064463	2018-11-16	13	2018-05-15	Aquamarine
1000000036	87	3842633393	2018-06-24	20	2018-10-04	Mauv
1000000037	88	5472116265	2018-12-11	16	2018-10-14	Blue
1000000038	89	9692759080	2018-08-12	5	2018-10-22	Yellow
1000000039	90	7514327435	2018-06-11	9	2018-10-11	Puce
1000000040	91	8681406680	2018-08-03	2	2018-11-15	Goldenrod
1000000041	92	3833595683	2018-08-04	6	2018-10-17	Aquamarine
1000000042	93	6495828210	2018-11-18	4	2018-01-07	Mauv
1000000043	94	6671249147	2018-06-20	20	2019-01-19	Blue
1000000044	95	1625709193	2018-07-06	20	2018-10-30	Yellow
1000000045	96	2971322039	2018-04-26	15	2018-01-16	Puce
1000000046	97	6671464639	2018-12-15	16	2018-12-03	Goldenrod
1000000047	98	9937647969	2018-11-09	3	2018-08-15	Aquamarine
1000000048	99	6691859401	2018-10-01	14	2018-10-31	Mauv
1000000049	100	9843758873	2018-09-04	10	2018-12-23	Blue
\.


--
-- Data for Name: chat; Type: TABLE DATA; Schema: toys_rent; Owner: db2018039
--

COPY toys_rent.chat (id, pesan, datetime, no_ktp_anggota, no_ktp_admin) FROM stdin;
267766071820860	nec, mollis vitae, posuere at,	2019-12-17 13:51:34	769-56-7751	169-27-1161
80992065274370	non	2019-03-06 03:17:51	221-11-0887	244-54-0862
796573473270581	est, congue a, aliquet vel, vulputate eu, odio. Phasellus at	2018-08-17 11:00:54	824-26-5666	169-27-1161
879287182843657	eget lacus. Mauris non	2019-06-22 02:31:50	221-11-0887	210-67-5616
446334987038778	Suspendisse aliquet, sem ut cursus	2019-09-06 00:13:25	209-72-6561	210-67-5616
948805055508084	eros non enim commodo hendrerit. Donec	2018-08-13 12:35:18	566-74-2960	415-88-6845
23487280996565	pretium aliquet, metus urna convallis erat,	2019-08-29 19:27:02	704-56-0789	210-67-5616
820377700125219	egestas. Fusce	2018-08-27 17:22:36	787-16-0278	434-83-5211
34053304128008	in lobortis	2019-08-05 14:26:25	300-71-7963	840-98-9871
534918053246679	mi lacinia mattis. Integer eu lacus. Quisque imperdiet, erat	2018-11-20 21:09:59	219-07-7284	434-83-5211
54176651088342	malesuada. Integer id magna	2019-11-02 02:13:28	835-85-7566	169-27-1161
493404137005941	tempus mauris	2019-02-12 03:58:56	787-55-8524	840-98-9871
626144088458766	Quisque ac libero nec ligula consectetuer	2018-05-05 16:11:42	525-13-4486	219-07-7284
499525492912550	Aenean egestas hendrerit neque. In ornare sagittis felis.	2019-05-02 01:42:17	829-09-8519	699-28-7792
35203628002076	luctus ut, pellentesque	2018-04-18 09:28:46	109-67-2867	434-83-5211
616814560070339	volutpat. Nulla facilisis. Suspendisse	2019-08-10 11:00:17	268-63-7402	210-67-5616
175515693392619	ipsum. Suspendisse	2019-07-16 11:21:47	704-02-7072	169-27-1161
416714028988706	et ipsum	2018-07-31 06:40:19	209-72-6561	415-88-6845
72011012915234	felis orci, adipiscing non, luctus sit amet, faucibus ut, nulla.	2019-09-28 03:17:44	518-75-8714	355-88-6116
126577786906066	egestas blandit. Nam nulla magna,	2019-11-24 05:04:07	330-43-7650	169-27-1161
116215466360749	nisl arcu iaculis enim, sit amet ornare lectus	2019-12-22 14:11:45	100-26-1701	169-27-1161
837155790872477	Fusce dolor quam, elementum at,	2018-06-12 12:49:03	704-56-0789	355-88-6116
970492646066934	Vivamus euismod urna. Nullam	2019-06-17 03:38:45	300-71-7963	769-54-4263
38767083466769	euismod et, commodo at, libero. Morbi	2019-11-01 05:41:51	210-67-5616	210-67-5616
89105123175697	ac	2019-07-10 22:44:33	787-16-0278	434-83-5211
753865595483493	Nullam vitae diam. Proin dolor. Nulla semper	2019-09-03 03:36:59	100-26-1701	219-07-7284
349130783330362	at sem molestie sodales.	2018-11-29 04:53:47	790-72-3809	244-54-0862
683492674992043	Nulla eget metus	2018-05-04 03:31:13	858-86-7166	355-88-6116
560363109262024	torquent per conubia nostra, per inceptos hymenaeos.	2020-04-05 13:40:09	811-34-4269	434-83-5211
361219804929869	velit in aliquet	2018-10-08 15:42:22	892-31-0530	415-88-6845
925384057465854	Phasellus dolor elit, pellentesque a, facilisis non, bibendum sed,	2019-11-24 02:44:42	171-49-4056	769-54-4263
77792860467365	Cras interdum. Nunc sollicitudin	2018-09-17 21:29:27	823-03-5197	169-27-1161
73892120861466	adipiscing, enim mi tempor	2018-11-30 05:11:32	320-64-4874	244-54-0862
416046531582728	egestas lacinia. Sed congue, elit sed consequat auctor, nunc nulla	2018-06-24 12:45:31	688-36-0929	219-07-7284
707102379970156	nec luctus felis purus ac tellus. Suspendisse	2020-03-22 17:16:48	672-01-3901	169-27-1161
703316253896749	aliquet. Phasellus fermentum convallis ligula.	2018-09-10 15:48:29	665-22-0469	434-83-5211
397330825844621	orci adipiscing. Mauris molestie	2018-11-08 17:41:15	704-56-0789	699-28-7792
700315762734997	libero at auctor ullamcorper, nisl arcu iaculis enim,	2019-02-19 19:24:36	671-42-8852	355-88-6116
262091705885442	enim  tempor	2019-05-06 03:25:22	518-75-8714	699-28-7792
82493281181650	Integer eu lacus. Quisque imperdiet, erat nonummy ultricies ornare, elit	2018-08-16 09:26:23	762-51-8697	434-83-5211
611041337348838	malesuada vel, venenatis	2018-12-20 12:38:32	546-70-8324	244-54-0862
407523887508094	Donec	2019-08-19 09:39:58	405-72-2139	355-88-6116
222177301958349	faucibus lectus, a sollicitudin orci sem eget massa. Suspendisse	2019-04-09 23:57:38	874-87-7806	434-83-5211
655690091689948	Donec tincidunt. Donec vitae	2019-04-20 16:31:33	518-75-8714	840-98-9871
397330825801621	orci tincidunt adipiscing. Mauris molestie	2018-11-08 16:41:15	704-56-0789	699-28-7792
262091705775442	enim mi tempor	2019-05-06 03:25:38	518-75-8714	699-28-7792
73561311291427	magnis dis parturient montes, nascetur ridiculus mus. Proin vel arcu	2019-09-23 14:03:27	824-26-5666	169-27-1161
821608664135220	auctor	2018-12-04 20:41:12	644-81-5787	169-27-1161
73565511291427	dis parturient montes, nascetur ridiculus mus. Proin vel arcu	2019-09-24 14:03:27	824-26-5666	169-27-1161
821602264135220	auctor pos	2018-12-04 20:41:11	644-81-5787	169-27-1161
\.


--
-- Data for Name: info_barang_level; Type: TABLE DATA; Schema: toys_rent; Owner: db2018039
--

COPY toys_rent.info_barang_level (id_barang, nama_level, harga_sewa, porsi_royalti) FROM stdin;
7134617969	GOLD	74525	35
6111727084	GOLD	71225	34
1181374463	GOLD	28123	23
1464127971	GOLD	26103	41
8742528960	GOLD	96613	49
9073852556	GOLD	28742	22
2861235396	GOLD	60435	32
7770956026	GOLD	62579	4
2674108313	GOLD	75957	28
4512712514	GOLD	16555	30
518613938	GOLD	72538	27
8231037796	GOLD	10814	50
9698400750	GOLD	51643	7
8964559265	GOLD	55886	25
4684114681	GOLD	96488	47
1892366027	GOLD	42046	41
8997806956	GOLD	79044	46
5245640631	GOLD	49193	14
3130407525	GOLD	84428	10
8213476364	GOLD	38308	21
4542464058	GOLD	99932	16
6066711389	GOLD	27621	15
2174254579	GOLD	81781	45
4809172419	GOLD	42140	27
8731118598	GOLD	59435	16
6018723876	GOLD	86929	5
7079656914	GOLD	95962	30
6301186623	GOLD	26231	49
7567807428	GOLD	83606	34
6642946488	GOLD	74667	8
2772047441	GOLD	76617	32
7126472604	GOLD	51901	4
4490966027	GOLD	51785	8
4952042477	GOLD	19892	28
7314675138	GOLD	36740	3
4245368459	GOLD	84028	44
1914046727	GOLD	46317	27
2235269854	GOLD	96288	43
4807782992	GOLD	93940	35
4473982818	GOLD	95958	14
6856134946	GOLD	38050	15
1490312514	GOLD	47748	29
9202009527	GOLD	75546	22
9074625352	GOLD	72198	6
2439945150	GOLD	39723	17
1213378830	GOLD	76700	19
7066353248	GOLD	19627	10
9834033716	GOLD	85164	7
3143442621	GOLD	63004	11
2418207600	GOLD	85228	1
1969545018	GOLD	31896	39
255967379	GOLD	79821	46
1630161595	GOLD	75681	29
8997075839	GOLD	85461	2
7717184818	GOLD	22000	27
5283217490	GOLD	16301	33
7877690412	GOLD	48750	48
4078492082	GOLD	75793	11
9182954088	GOLD	74582	33
572786714	GOLD	83411	41
5941951736	GOLD	52533	3
2086614043	GOLD	32687	46
9968669854	GOLD	15801	7
7753053445	GOLD	10445	40
7358907856	GOLD	60705	22
7276438752	GOLD	69427	37
7480190030	GOLD	91292	44
4323377206	GOLD	68364	7
2424345958	GOLD	81876	25
4465084451	GOLD	73073	27
3243846560	GOLD	44683	1
7628006728	GOLD	98643	27
2974716868	GOLD	77792	34
3276822725	GOLD	46651	46
2575675219	GOLD	20829	13
9731179877	GOLD	26351	31
8653893676	GOLD	66334	41
9606560746	GOLD	16722	48
8444396379	GOLD	28189	39
5912404602	GOLD	21758	43
4891587872	GOLD	66691	39
6844429861	GOLD	23734	3
3901826434	GOLD	38936	21
4506486746	GOLD	22233	3
1150732581	GOLD	65717	45
9100064463	GOLD	15226	18
3842633393	GOLD	95441	44
5472116265	GOLD	14845	45
9692759080	GOLD	68881	21
7514327435	GOLD	34611	47
8681406680	GOLD	75479	15
3833595683	GOLD	58750	47
6495828210	GOLD	70018	40
6671249147	GOLD	92929	8
1625709193	GOLD	11597	5
2971322039	GOLD	78284	16
6671464639	GOLD	91675	42
9937647969	GOLD	99137	50
6691859401	GOLD	61223	5
9843758873	GOLD	65365	35
7134617969	SILVER	75842	16
6111727084	SILVER	92732	33
1181374463	SILVER	16526	28
1464127971	SILVER	39029	8
8742528960	SILVER	94380	43
9073852556	SILVER	40092	39
2861235396	SILVER	76486	22
7770956026	SILVER	24051	17
2674108313	SILVER	54163	38
4512712514	SILVER	96322	21
518613938	SILVER	35521	11
8231037796	SILVER	67645	27
9698400750	SILVER	69380	41
8964559265	SILVER	19984	44
4684114681	SILVER	98213	8
1892366027	SILVER	90545	36
8997806956	SILVER	13345	42
5245640631	SILVER	34206	12
3130407525	SILVER	57652	43
8213476364	SILVER	49168	40
4542464058	SILVER	29812	32
6066711389	SILVER	92130	13
2174254579	SILVER	31439	12
4809172419	SILVER	28081	3
8731118598	SILVER	46634	31
6018723876	SILVER	34019	25
7079656914	SILVER	46184	41
6301186623	SILVER	22247	41
7567807428	SILVER	34125	16
6642946488	SILVER	51436	32
2772047441	SILVER	98853	49
7126472604	SILVER	88990	49
4490966027	SILVER	40015	38
4952042477	SILVER	83763	7
7314675138	SILVER	56587	10
4245368459	SILVER	80361	23
1914046727	SILVER	84538	35
2235269854	SILVER	17275	14
4807782992	SILVER	32825	37
4473982818	SILVER	73714	27
6856134946	SILVER	48542	50
1490312514	SILVER	77292	27
9202009527	SILVER	25190	22
9074625352	SILVER	54148	41
2439945150	SILVER	96821	35
1213378830	SILVER	83725	34
7066353248	SILVER	55922	21
9834033716	SILVER	59191	15
3143442621	SILVER	10361	9
2418207600	SILVER	21044	23
1969545018	SILVER	28657	42
255967379	SILVER	33520	13
1630161595	SILVER	76422	48
8997075839	SILVER	99038	21
7717184818	SILVER	21470	39
5283217490	SILVER	28004	1
7877690412	SILVER	98799	28
4078492082	SILVER	69700	9
9182954088	SILVER	74792	2
572786714	SILVER	40850	4
5941951736	SILVER	95781	43
2086614043	SILVER	91424	11
9968669854	SILVER	65542	50
7753053445	SILVER	59639	31
7358907856	SILVER	20633	33
7276438752	SILVER	24161	8
7480190030	SILVER	39215	49
4323377206	SILVER	63166	9
2424345958	SILVER	65307	4
4465084451	SILVER	37129	11
3243846560	SILVER	72918	20
7628006728	SILVER	69191	21
2974716868	SILVER	26739	47
3276822725	SILVER	34435	43
2575675219	SILVER	65624	19
9731179877	SILVER	81423	42
8653893676	SILVER	25829	36
9606560746	SILVER	11411	5
8444396379	SILVER	91745	35
5912404602	SILVER	16098	31
4891587872	SILVER	50384	32
6844429861	SILVER	42789	45
3901826434	SILVER	74785	38
4506486746	SILVER	45741	31
1150732581	SILVER	64230	48
9100064463	SILVER	33402	26
3842633393	SILVER	55216	18
5472116265	SILVER	78578	20
9692759080	SILVER	60085	25
7514327435	SILVER	27458	31
8681406680	SILVER	13686	35
3833595683	SILVER	66374	42
6495828210	SILVER	54387	13
6671249147	SILVER	99446	37
1625709193	SILVER	88977	40
2971322039	SILVER	13812	40
6671464639	SILVER	67285	34
9937647969	SILVER	31226	16
6691859401	SILVER	96582	7
9843758873	SILVER	61551	48
7134617969	BRONZE	38714	1
6111727084	BRONZE	54230	16
1181374463	BRONZE	25756	8
1464127971	BRONZE	98247	17
8742528960	BRONZE	79826	26
9073852556	BRONZE	28614	6
2861235396	BRONZE	24126	16
7770956026	BRONZE	97616	50
2674108313	BRONZE	56582	27
4512712514	BRONZE	80260	32
518613938	BRONZE	38379	48
8231037796	BRONZE	74604	32
9698400750	BRONZE	84022	42
8964559265	BRONZE	18181	35
4684114681	BRONZE	84528	5
1892366027	BRONZE	33118	33
8997806956	BRONZE	62300	11
5245640631	BRONZE	78047	47
3130407525	BRONZE	41536	5
8213476364	BRONZE	33919	5
4542464058	BRONZE	86775	29
6066711389	BRONZE	85259	45
2174254579	BRONZE	64334	5
4809172419	BRONZE	55415	44
8731118598	BRONZE	57893	8
6018723876	BRONZE	84185	15
7079656914	BRONZE	45806	49
6301186623	BRONZE	36654	36
7567807428	BRONZE	58462	3
6642946488	BRONZE	27920	34
2772047441	BRONZE	87749	13
7126472604	BRONZE	13163	44
4490966027	BRONZE	61663	25
4952042477	BRONZE	83219	43
7314675138	BRONZE	37711	24
4245368459	BRONZE	96175	17
1914046727	BRONZE	90674	8
2235269854	BRONZE	18465	19
4807782992	BRONZE	46171	17
4473982818	BRONZE	91031	23
6856134946	BRONZE	91334	28
1490312514	BRONZE	55963	36
9202009527	BRONZE	49951	50
9074625352	BRONZE	38927	31
2439945150	BRONZE	90904	16
1213378830	BRONZE	74783	42
7066353248	BRONZE	87511	32
9834033716	BRONZE	87740	34
3143442621	BRONZE	60598	36
2418207600	BRONZE	13116	28
1969545018	BRONZE	95408	42
255967379	BRONZE	59701	27
1630161595	BRONZE	71500	21
8997075839	BRONZE	56650	14
7717184818	BRONZE	74715	9
5283217490	BRONZE	27404	31
7877690412	BRONZE	14576	5
4078492082	BRONZE	23689	12
9182954088	BRONZE	89975	45
572786714	BRONZE	24436	33
5941951736	BRONZE	98442	5
2086614043	BRONZE	12802	34
9968669854	BRONZE	64141	47
7753053445	BRONZE	34910	11
7358907856	BRONZE	37461	40
7276438752	BRONZE	79809	43
7480190030	BRONZE	71323	6
4323377206	BRONZE	77880	5
2424345958	BRONZE	26197	1
4465084451	BRONZE	58586	39
3243846560	BRONZE	98277	16
7628006728	BRONZE	42155	33
2974716868	BRONZE	77019	6
3276822725	BRONZE	15410	15
2575675219	BRONZE	43992	39
9731179877	BRONZE	71965	21
8653893676	BRONZE	65950	31
9606560746	BRONZE	49377	48
8444396379	BRONZE	82228	49
5912404602	BRONZE	39876	7
4891587872	BRONZE	40926	35
6844429861	BRONZE	27666	10
3901826434	BRONZE	46327	16
4506486746	BRONZE	69597	2
1150732581	BRONZE	76402	4
9100064463	BRONZE	48827	44
3842633393	BRONZE	76702	21
5472116265	BRONZE	36655	15
9692759080	BRONZE	88018	19
7514327435	BRONZE	40473	46
8681406680	BRONZE	72284	39
3833595683	BRONZE	15652	34
6495828210	BRONZE	38268	6
6671249147	BRONZE	35701	22
1625709193	BRONZE	45165	26
2971322039	BRONZE	60653	28
6671464639	BRONZE	82307	11
9937647969	BRONZE	62345	1
6691859401	BRONZE	40496	30
9843758873	BRONZE	54109	22
\.


--
-- Data for Name: item; Type: TABLE DATA; Schema: toys_rent; Owner: db2018039
--

COPY toys_rent.item (nama, deskripsi, usia_dari, usia_sampai, bahan) FROM stdin;
a	pede blandit congue. In	1	10	mi, ac mattis
ac	dui. Cras pellentesque. Sed dictum.	5	12	gravida sagittis. Duis
et	aliquet. Phasellus fermentum convallis ligula.	5	8	non dui
velit.	Aliquam	5	11	ac, fermentum
Mauris	Phasellus ornare.	1	9	velit eu
fermentum	dolor dolor, tempus	4	10	metus sit
malesuada	eu	3	11	hymenaeos.
enim.	sem magna nec quam.	2	8	penatibus et
cursus,	eget odio. Aliquam vulputate	4	11	ligula
arcu.	faucibus orci	1	8	dis
luctus	ligula. Aliquam erat	2	10	at
commodo	vehicula risus. Nulla eget	5	6	magna a tortor.
Phasellus	felis	5	10	tempus mauris
ssss	lobortis tellus justo	2	10	risus varius orci,
a,	nonummy	5	9	et, rutrum
volutpat.	velit	2	6	odio a purus.
purus.	consectetuer adipiscing elit. Curabitur sed	3	6	nibh
ridiculus	Maecenas ornare	2	11	eget
erat	accumsan interdum libero dui nec	1	11	cursus. Integer
eu	luctus ut, pellentesque	5	12	urna
mi	pede. Suspendisse dui. Fusce diam	1	8	tempus eu,
consectetuer,	aliquet vel,	5	8	adipiscing elit. Etiam
cursus.	id magna	4	11	aliquet, metus
mauris,	urna justo	5	6	Cras convallis
purus	facilisis	5	6	dui, nec tempus
\.


--
-- Data for Name: kategori; Type: TABLE DATA; Schema: toys_rent; Owner: db2018039
--

COPY toys_rent.kategori (nama, level, sub_dari) FROM stdin;
plastik	0	\N
kayu	1	plastik
besi	2	kayu
stainless	0	\N
karet	0	\N
\.


--
-- Data for Name: kategori_item; Type: TABLE DATA; Schema: toys_rent; Owner: db2018039
--

COPY toys_rent.kategori_item (nama_item, nama_kategori) FROM stdin;
a	plastik
ac	plastik
et	plastik
velit.	plastik
Mauris	plastik
fermentum	plastik
malesuada	plastik
enim.	plastik
cursus,	plastik
arcu.	plastik
luctus	plastik
commodo	plastik
Phasellus	plastik
ssss	plastik
a,	plastik
volutpat.	plastik
purus.	plastik
ridiculus	plastik
erat	plastik
eu	plastik
mi	plastik
consectetuer,	plastik
cursus.	plastik
mauris,	plastik
purus	plastik
a	kayu
ac	kayu
et	kayu
velit.	kayu
Mauris	kayu
fermentum	kayu
malesuada	kayu
enim.	kayu
cursus,	kayu
arcu.	kayu
luctus	kayu
commodo	kayu
Phasellus	kayu
ssss	kayu
a,	kayu
volutpat.	kayu
purus.	kayu
ridiculus	kayu
erat	kayu
eu	kayu
mi	kayu
consectetuer,	kayu
cursus.	kayu
mauris,	kayu
purus	kayu
a	besi
ac	besi
et	besi
velit.	besi
Mauris	besi
fermentum	karet
malesuada	karet
enim.	karet
cursus,	karet
arcu.	karet
luctus	karet
commodo	karet
Phasellus	karet
ssss	karet
a,	karet
volutpat.	karet
purus.	karet
ridiculus	karet
erat	karet
eu	karet
mi	karet
consectetuer,	karet
cursus.	karet
mauris,	karet
purus	stainless
\.


--
-- Data for Name: level_keanggotaan; Type: TABLE DATA; Schema: toys_rent; Owner: db2018039
--

COPY toys_rent.level_keanggotaan (nama_level, minimum_poin, deskripsi) FROM stdin;
BRONZE	20	Level Bronze
SILVER	50	Level Silver
test	1500	testing
testdua	2000	testingdua
testingtiga	3000	testingtiga
GOLD	165	Level Gold
\.


--
-- Data for Name: pemesanan; Type: TABLE DATA; Schema: toys_rent; Owner: db2018039
--

COPY toys_rent.pemesanan (id_pemesanan, datetime_pesanan, kuantitas_barang, harga_sewa, ongkos, no_ktp_pemesan, status) FROM stdin;
1000000000	2018-10-03 00:00:00	26	53457	58339	874-87-7806	Mauv
1000000001	2019-02-24 00:00:00	2	47459	83804	762-51-8697	Blue
1000000002	2019-04-10 00:00:00	37	81952	80923	500-28-0196	Yellow
1000000003	2018-08-11 00:00:00	51	97569	4408	858-86-7166	Puce
1000000004	2019-03-30 00:00:00	75	22159	17495	197-59-5675	Goldenrod
1000000005	2019-01-19 00:00:00	5	91202	21567	892-31-0530	Aquamarine
1000000006	2018-04-14 00:00:00	32	12607	83111	854-96-8799	Mauv
1000000007	2018-09-09 00:00:00	54	59008	37708	566-74-2960	Blue
1000000008	2018-04-28 00:00:00	24	4079	50372	571-17-7573	Yellow
1000000009	2018-05-28 00:00:00	68	34270	32379	119-70-1944	Puce
1000000010	2019-01-20 00:00:00	46	77640	43208	268-63-7402	Goldenrod
1000000011	2018-04-15 00:00:00	78	85821	32687	120-80-8048	Aquamarine
1000000012	2018-07-02 00:00:00	6	2138	10753	665-22-0469	Mauv
1000000013	2018-05-13 00:00:00	46	66205	78806	787-55-8524	Blue
1000000014	2018-06-04 00:00:00	47	65384	36177	371-02-2990	Yellow
1000000015	2018-11-30 00:00:00	33	47123	39524	405-72-2139	Puce
1000000016	2018-08-29 00:00:00	42	82268	13135	787-16-0278	Goldenrod
1000000017	2018-06-06 00:00:00	56	87097	11570	198-72-3705	Aquamarine
1000000018	2019-02-08 00:00:00	42	9122	20264	775-59-9748	Mauv
1000000019	2018-08-14 00:00:00	14	9529	54610	565-09-6695	Blue
1000000020	2019-04-05 00:00:00	44	78295	28232	838-20-0423	Yellow
1000000021	2018-04-27 00:00:00	89	46775	25539	511-41-6666	Puce
1000000022	2018-07-01 00:00:00	10	6773	21366	835-85-7566	Goldenrod
1000000023	2018-12-18 00:00:00	74	11385	18978	171-49-4056	Aquamarine
1000000024	2019-03-16 00:00:00	20	20144	72037	769-56-7751	Mauv
1000000025	2019-03-23 00:00:00	100	31684	50695	839-04-6798	Blue
1000000026	2018-09-15 00:00:00	91	12622	40226	564-76-8185	Yellow
1000000027	2019-01-21 00:00:00	21	10662	60599	320-64-4874	Puce
1000000028	2018-12-10 00:00:00	17	2883	62640	803-37-2157	Goldenrod
1000000029	2019-02-01 00:00:00	26	60973	75739	486-08-5371	Aquamarine
1000000030	2018-08-26 00:00:00	96	60601	43773	803-96-7763	Mauv
1000000031	2019-01-10 00:00:00	58	9423	7152	557-91-8304	Blue
1000000032	2018-09-04 00:00:00	85	20757	3504	331-65-3270	Yellow
1000000033	2018-10-20 00:00:00	59	46761	94732	586-61-3217	Puce
1000000034	2018-10-04 00:00:00	69	34443	14375	221-11-0887	Goldenrod
1000000035	2019-02-02 00:00:00	21	11328	87798	628-24-4154	Aquamarine
1000000036	2019-01-29 00:00:00	78	88902	90275	824-26-5666	Mauv
1000000037	2019-01-26 00:00:00	10	36832	86527	829-09-8519	Blue
1000000038	2018-09-14 00:00:00	75	29828	93571	546-70-8324	Yellow
1000000039	2018-09-11 00:00:00	75	18969	28532	481-50-3155	Puce
1000000040	2018-08-28 00:00:00	16	55678	27962	790-72-3809	Goldenrod
1000000041	2019-01-09 00:00:00	25	38045	76900	169-27-1161	Aquamarine
1000000042	2018-06-23 00:00:00	43	7217	61366	840-98-9871	Mauv
1000000043	2018-11-01 00:00:00	97	70089	92003	553-68-6779	Blue
1000000044	2018-12-11 00:00:00	1	71179	6022	118-16-1795	Yellow
1000000045	2019-02-23 00:00:00	60	41236	96454	200-89-0821	Puce
1000000046	2019-02-24 00:00:00	91	78155	24820	495-72-7317	Goldenrod
1000000047	2018-10-09 00:00:00	70	54432	87591	140-84-7136	Aquamarine
1000000048	2018-11-22 00:00:00	25	91822	28762	771-55-2007	Mauv
1000000049	2018-06-10 00:00:00	26	28220	1376	526-33-5938	Blue
\.


--
-- Data for Name: pengembalian; Type: TABLE DATA; Schema: toys_rent; Owner: db2018039
--

COPY toys_rent.pengembalian (no_resi, id_pemesanan, metode, ongkos, tanggal, no_ktp_anggota, nama_alamat_anggota) FROM stdin;
2982131805	1000000000	Macropus agilis	69243	2019-01-07	769-54-4263	Simeon Well
2177001304	1000000001	unavailable	54145	2018-12-10	244-54-0862	Ruggiero Schultze
2510042057	1000000002	Plectopterus gambensis	63035	2018-07-14	169-27-1161	Marita Menichini
2664582539	1000000003	Speotyte cuniculata	21238	2018-06-24	415-88-6845	Derrik Smullen
2635345491	1000000004	Limnocorax flavirostra	35534	2019-02-23	219-07-7284	Ellerey De Vuyst
2268968038	1000000005	Semnopithecus entellus	2106	2018-09-08	699-28-7792	Clo Balducci
2038788439	1000000006	Eolophus roseicapillus	11623	2018-05-19	434-83-5211	Allan McClounan
2843248633	1000000007	Hystrix cristata	36936	2018-06-02	840-98-9871	Garrett Delagnes
2661098325	1000000008	Eolophus roseicapillus	64656	2018-08-29	210-67-5616	Drake Morland
2041974134	1000000009	Cygnus atratus	51861	2018-12-01	355-88-6116	Misty Penrith
2386527035	1000000010	Canis aureus	76816	2018-08-14	830-50-3193	Aloin Copcutt
2932342209	1000000011	Propithecus verreauxi	26678	2018-08-07	520-55-6251	Barton Sprigin
2753411573	1000000012	Sula dactylatra	98676	2019-03-31	866-89-7074	Zora Rentilll
2509636395	1000000013	Graspus graspus	63737	2018-12-05	688-36-0929	Marmaduke McKleod
2142870573	1000000014	Felis concolor	24986	2018-11-28	330-43-7650	Kandy Kroger
2063896362	1000000015	Uraeginthus granatina	68698	2018-12-02	481-99-4642	Ulrich Nern
2526272269	1000000016	unavailable	88091	2018-12-10	704-02-7072	Laurie Kellie
2781987358	1000000017	Eubalaena australis	55791	2018-04-14	818-63-8795	Delmore Royal
2799899838	1000000018	Macropus fuliginosus	12053	2018-05-29	553-68-6779	Lanny Grouen
2963444546	1000000019	Canis lupus lycaon	21833	2018-05-12	525-13-4486	Natalee Grassin
\.


--
-- Data for Name: pengguna; Type: TABLE DATA; Schema: toys_rent; Owner: db2018039
--

COPY toys_rent.pengguna (no_ktp, nama_lengkap, email, tanggal_lahir, no_telp) FROM stdin;
769-54-4263	Simeon Well	swell0@hp.com	1991-04-24	7513029680
244-54-0862	Ruggiero Schultze	rschultze1@salon.com	2002-10-16	1265362489
169-27-1161	Marita Menichini	mmenichini2@flavors.me	2009-02-01	9851945404
415-88-6845	Derrik Smullen	dsmullen3@over-blog.com	1999-05-13	8791559294
219-07-7284	Ellerey De Vuyst	ede4@toplist.cz	2006-02-19	3965247217
699-28-7792	Clo Balducci	cbalducci5@ucoz.ru	1986-08-09	2154272745
434-83-5211	Allan McClounan	amcclounan6@usatoday.com	1981-12-18	8894791608
840-98-9871	Garrett Delagnes	gdelagnes7@mayoclinic.com	2001-01-12	9241652007
210-67-5616	Drake Morland	dmorland8@exblog.jp	2007-03-01	5795435228
355-88-6116	Misty Penrith	mpenrith9@tripod.com	1993-08-27	1608319532
830-50-3193	Aloin Copcutt	acopcutta@telegraph.co.uk	1994-06-15	8608124187
520-55-6251	Barton Sprigin	bspriginb@cornell.edu	2001-06-13	3895966954
866-89-7074	Zora Rentilll	zrentilllc@instagram.com	1991-08-09	8557853270
688-36-0929	Marmaduke McKleod	mmckleodd@merriam-webster.com	2003-11-23	1349697586
330-43-7650	Kandy Kroger	kkrogere@shinystat.com	1984-07-27	6208038029
481-99-4642	Ulrich Nern	unernf@census.gov	1984-01-18	7093193815
704-02-7072	Laurie Kellie	lkellieg@google.com	2010-01-02	3279128949
818-63-8795	Delmore Royal	droyalh@google.it	1994-09-16	9805404896
553-68-6779	Lanny Grouen	lgroueni@wikimedia.org	2005-12-25	7871873102
525-13-4486	Natalee Grassin	ngrassinj@illinois.edu	2010-01-02	6948650160
356-50-2151	Nadine Eatherton	neathertonk@adobe.com	1988-11-15	2291868403
808-77-0426	Creighton Beushaw	cbeushawl@163.com	2001-05-24	1514212157
428-77-4060	Cecily Rubinow	crubinowm@facebook.com	1986-10-05	7184511777
109-67-2867	Jane Candish	jcandishn@washingtonpost.com	1998-04-25	7832810079
644-81-5787	Daren Raraty	draratyo@ucoz.com	1982-02-02	9516503158
115-06-0419	Shari Hannum	shannump@ebay.co.uk	2004-05-18	7301899523
118-16-1795	Wynny Spatig	wspatigq@dion.ne.jp	1993-12-01	8053866792
236-16-1747	Theresina Fitter	tfitterr@sitemeter.com	2002-09-16	4555650660
647-84-9703	Janaye Goodisson	jgoodissons@hubpages.com	2007-09-14	1744830906
300-71-7963	Brady Sarfatti	bsarfattit@vimeo.com	1987-07-10	1959543896
672-01-3901	Iago Chasmer	ichasmeru@upenn.edu	2008-08-16	1817072215
823-03-5197	Tibold Chamney	tchamneyv@china.com.cn	1997-07-26	4968117424
187-32-5699	Nonna Randleson	nrandlesonw@eepurl.com	2006-07-24	7929581156
456-74-5371	Selina Scotti	sscottix@topsy.com	2006-12-25	8005526657
443-25-5572	Leo Lay	llayy@msu.edu	1991-08-01	9936805631
495-74-7629	Jasmin Danieli	jdanieliz@engadget.com	1984-08-24	3888695308
671-42-8852	Aeriel Griniov	agriniov10@hibu.com	1992-06-06	6776765468
284-14-9248	Isa Smallacombe	ismallacombe11@trellian.com	2006-07-25	3023270964
821-83-7217	Eugine Welsh	ewelsh12@prnewswire.com	1980-06-14	9469686686
359-09-0091	Yolane Tows	ytows13@purevolume.com	2009-02-07	5191401847
518-75-8714	George Marcq	gmarcq14@t-online.de	2004-07-19	7744420802
811-34-4269	Bethanne Cutridge	bcutridge15@washington.edu	2002-07-10	8848203928
864-69-9469	Gregg Rummins	grummins16@blogger.com	2000-07-08	4522700858
457-51-9515	Blondelle Strephan	bstrephan17@army.mil	1983-06-14	9922218030
100-26-1701	Sampson Neubigging	sneubigging18@creativecommons.org	2004-10-26	8277551844
704-56-0789	Bonnie Petricek	bpetricek19@intel.com	1986-11-17	1338249755
769-01-4151	Velma Duligal	vduligal1a@ifeng.com	2003-11-17	1225884148
649-13-3997	Ophelie Winsborrow	owinsborrow1b@businessweek.com	2000-09-17	2853373707
279-68-6913	Lovell Semorad	lsemorad1c@yolasite.com	1990-06-22	3359450758
209-72-6561	Bevan Harrold	bharrold1d@blogger.com	1997-03-14	6788081945
865-61-1176	Jules Yakovich	jyakovich1e@china.com.cn	1990-03-06	9854946127
880-19-4901	Michale Kirke	mkirke1f@free.fr	1984-09-29	4725627265
679-87-9655	Nicki Palethorpe	npalethorpe1g@ustream.tv	1999-10-19	3063661030
874-87-7806	Shalne McCorry	smccorry1h@xinhuanet.com	2002-12-11	7659097011
762-51-8697	Lowrance Bausmann	lbausmann1i@xinhuanet.com	1991-02-02	6798270369
500-28-0196	Gretel Yushkov	gyushkov1j@economist.com	1983-03-09	1773006333
858-86-7166	Briana Risely	brisely1k@eventbrite.com	1997-09-13	8268627867
200-89-0821	Veronika Newlin	vnewlin1l@miitbeian.gov.cn	1988-10-20	9718466918
197-59-5675	Hildagarde Burbage	hburbage1m@dyndns.org	1992-08-10	2053116148
892-31-0530	Avis Harbron	aharbron1n@usatoday.com	1991-09-12	8306119815
854-96-8799	Otes Brien	obrien1o@dedecms.com	2002-01-02	9601199533
566-74-2960	Dolf Corradetti	dcorradetti1p@un.org	1989-09-03	3044726792
571-17-7573	Stephine Yewdale	syewdale1q@timesonline.co.uk	1997-10-10	1017175583
119-70-1944	Cass Langridge	clangridge1r@mlb.com	1985-02-22	8623826542
268-63-7402	Les Schutte	lschutte1s@merriam-webster.com	2007-04-22	7748023953
495-72-7317	Pepillo MacShirrie	pmacshirrie1t@intel.com	2005-09-03	1409269779
120-80-8048	Roger Ullyott	rullyott1u@wunderground.com	2008-06-11	4707052916
665-22-0469	Timmy Iacovuzzi	tiacovuzzi1v@meetup.com	1986-05-11	5775829913
787-55-8524	Charissa Varley	cvarley1w@gizmodo.com	2006-09-13	6384202547
371-02-2990	Aubrie McPeice	amcpeice1x@businessweek.com	1995-07-26	7464231542
405-72-2139	Lenora Ranger	lranger1y@squidoo.com	2003-07-14	6644713540
787-16-0278	Lil Cayle	lcayle1z@noaa.gov	1982-03-15	8166152514
198-72-3705	Eugenia Masdin	emasdin20@timesonline.co.uk	1983-01-22	1031498757
775-59-9748	Reta Willcot	rwillcot21@narod.ru	1988-06-17	5701438546
565-09-6695	Briny Battista	bbattista22@intel.com	1995-09-23	9241038401
838-20-0423	Cele Gilvary	cgilvary23@salon.com	1982-01-22	4773069365
511-41-6666	Maxine Rate	mrate24@google.com	1982-02-02	3961051342
140-84-7136	Avrit Hinckes	ahinckes25@dell.com	1993-07-26	1764918540
835-85-7566	Silvester Beynon	sbeynon26@illinois.edu	1993-04-26	5274066733
171-49-4056	Anjela Ibberson	aibberson27@i2i.jp	2000-02-03	1485226688
771-55-2007	Mordy Scutter	mscutter28@unesco.org	2001-05-09	6786588908
769-56-7751	Artair Eberle	aeberle29@smugmug.com	1983-11-18	3026519926
839-04-6798	Rubie Bray	rbray2a@netscape.com	1990-06-14	3153847533
564-76-8185	Lynsey Jakubovsky	ljakubovsky2b@skype.com	1986-05-23	6952715015
320-64-4874	Rolland Essex	ressex2c@intel.com	1991-08-17	5912101296
803-37-2157	Caril Karslake	ckarslake2d@cargocollective.com	1999-04-05	1185627990
486-08-5371	Aurelie Murrthum	amurrthum2e@cocolog-nifty.com	1980-11-08	2391804820
803-96-7763	Hadleigh Dany	hdany2f@samsung.com	1988-06-15	4191326695
557-91-8304	Jacynth Mathivet	jmathivet2g@bandcamp.com	1989-09-20	6003754386
331-65-3270	Nikos Stuttard	nstuttard2h@mysql.com	2002-10-29	3162736402
586-61-3217	Pete Nial	pnial2i@noaa.gov	2009-05-02	1748104947
526-33-5938	Nap Kuhnt	nkuhnt2j@mac.com	1990-09-18	5604130830
221-11-0887	Gabey Harburtson	gharburtson2k@1und1.de	2003-09-05	4092851017
628-24-4154	Amitie Heinish	aheinish2l@ameblo.jp	2003-03-12	7825289299
824-26-5666	Sukey Toland	stoland2m@icq.com	1982-03-17	6176553912
628-57-6193	Jaine Cusick	jcusick2n@list-manage.com	2003-05-31	7835054032
829-09-8519	Karly Twitchings	ktwitchings2o@1und1.de	1981-04-12	5294051998
546-70-8324	Sollie Hutchings	shutchings2p@wisc.edu	1998-09-16	4502626855
481-50-3155	Ban Meale	bmeale2q@house.gov	2002-07-28	7042363489
790-72-3809	Joby Colmer	jcolmer2r@sohu.com	1985-06-16	4048275830
1234	test	test@mail.com	\N	\N
\.


--
-- Data for Name: pengiriman; Type: TABLE DATA; Schema: toys_rent; Owner: db2018039
--

COPY toys_rent.pengiriman (no_resi, id_pemesanan, metode, ongkos, tanggal, no_ktp_anggota, nama_alamat_anggota) FROM stdin;
2982131805	1000000000	Macropus agilis	20272	2019-02-17	769-54-4263	Simeon Well
2177001304	1000000001	unavailable	3427	2018-05-20	244-54-0862	Ruggiero Schultze
2510042057	1000000002	Plectopterus gambensis	18808	2018-08-06	169-27-1161	Marita Menichini
2664582539	1000000003	Speotyte cuniculata	58779	2019-03-26	415-88-6845	Derrik Smullen
2635345491	1000000004	Limnocorax flavirostra	66824	2018-07-17	219-07-7284	Ellerey De Vuyst
2268968038	1000000005	Semnopithecus entellus	45114	2018-10-14	699-28-7792	Clo Balducci
2038788439	1000000006	Eolophus roseicapillus	26265	2018-04-18	434-83-5211	Allan McClounan
2843248633	1000000007	Hystrix cristata	33849	2019-02-16	840-98-9871	Garrett Delagnes
2661098325	1000000008	Eolophus roseicapillus	70600	2018-07-30	210-67-5616	Drake Morland
2041974134	1000000009	Cygnus atratus	24868	2018-08-23	355-88-6116	Misty Penrith
2386527035	1000000010	Canis aureus	7932	2019-01-27	830-50-3193	Aloin Copcutt
2932342209	1000000011	Propithecus verreauxi	27563	2018-12-29	520-55-6251	Barton Sprigin
2753411573	1000000012	Sula dactylatra	30538	2018-06-01	866-89-7074	Zora Rentilll
2509636395	1000000013	Graspus graspus	42335	2019-03-15	688-36-0929	Marmaduke McKleod
2142870573	1000000014	Felis concolor	98493	2018-05-25	330-43-7650	Kandy Kroger
2063896362	1000000015	Uraeginthus granatina	70658	2018-09-08	481-99-4642	Ulrich Nern
2526272269	1000000016	unavailable	10946	2019-01-12	704-02-7072	Laurie Kellie
2781987358	1000000017	Eubalaena australis	89488	2018-09-11	818-63-8795	Delmore Royal
2799899838	1000000018	Macropus fuliginosus	66029	2018-08-16	553-68-6779	Lanny Grouen
2963444546	1000000019	Canis lupus lycaon	43604	2018-12-26	525-13-4486	Natalee Grassin
\.


--
-- Data for Name: status; Type: TABLE DATA; Schema: toys_rent; Owner: db2018039
--

COPY toys_rent.status (nama, deskripsi) FROM stdin;
Mauv	Open wound chest-compl
Blue	Oligohydramnios aff NB
Yellow	Periostitis-forearm
Puce	Social conduct dis-unsp
Goldenrod	Fetal death-anoxia NOS
Aquamarine	Ob pyemic embol-postpart
\.


--
-- Name: admin admin_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.admin
    ADD CONSTRAINT admin_pkey PRIMARY KEY (no_ktp);


--
-- Name: alamat alamat_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.alamat
    ADD CONSTRAINT alamat_pkey PRIMARY KEY (no_ktp_anggota, nama);


--
-- Name: anggota anggota_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.anggota
    ADD CONSTRAINT anggota_pkey PRIMARY KEY (no_ktp);


--
-- Name: barang_dikembalikan barang_dikembalikan_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.barang_dikembalikan
    ADD CONSTRAINT barang_dikembalikan_pkey PRIMARY KEY (no_resi, no_urut);


--
-- Name: barang_dikirim barang_dikirim_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.barang_dikirim
    ADD CONSTRAINT barang_dikirim_pkey PRIMARY KEY (no_resi, no_urut);


--
-- Name: barang_pesanan barang_pesanan_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.barang_pesanan
    ADD CONSTRAINT barang_pesanan_pkey PRIMARY KEY (id_pemesanan, no_urut);


--
-- Name: barang barang_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.barang
    ADD CONSTRAINT barang_pkey PRIMARY KEY (id_barang);


--
-- Name: chat chat_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.chat
    ADD CONSTRAINT chat_pkey PRIMARY KEY (id);


--
-- Name: info_barang_level info_barang_level_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.info_barang_level
    ADD CONSTRAINT info_barang_level_pkey PRIMARY KEY (id_barang, nama_level);


--
-- Name: item item_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.item
    ADD CONSTRAINT item_pkey PRIMARY KEY (nama);


--
-- Name: kategori_item kategori_item_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.kategori_item
    ADD CONSTRAINT kategori_item_pkey PRIMARY KEY (nama_item, nama_kategori);


--
-- Name: kategori kategori_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.kategori
    ADD CONSTRAINT kategori_pkey PRIMARY KEY (nama);


--
-- Name: level_keanggotaan level_keanggotaan_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.level_keanggotaan
    ADD CONSTRAINT level_keanggotaan_pkey PRIMARY KEY (nama_level);


--
-- Name: pemesanan pemesanan_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.pemesanan
    ADD CONSTRAINT pemesanan_pkey PRIMARY KEY (id_pemesanan);


--
-- Name: pengembalian pengembalian_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.pengembalian
    ADD CONSTRAINT pengembalian_pkey PRIMARY KEY (no_resi);


--
-- Name: pengguna pengguna_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.pengguna
    ADD CONSTRAINT pengguna_pkey PRIMARY KEY (no_ktp);


--
-- Name: pengiriman pengiriman_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.pengiriman
    ADD CONSTRAINT pengiriman_pkey PRIMARY KEY (no_resi);


--
-- Name: status status_pkey; Type: CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (nama);


--
-- Name: pemesanan add_points; Type: TRIGGER; Schema: toys_rent; Owner: db2018039
--

CREATE TRIGGER add_points AFTER INSERT ON toys_rent.pemesanan FOR EACH ROW EXECUTE PROCEDURE toys_rent.add_points_pemesanan();


--
-- Name: barang add_points; Type: TRIGGER; Schema: toys_rent; Owner: db2018039
--

CREATE TRIGGER add_points AFTER INSERT ON toys_rent.barang FOR EACH ROW EXECUTE PROCEDURE toys_rent.add_points_barang();


--
-- Name: anggota cek_level_tambah_poin_trig; Type: TRIGGER; Schema: toys_rent; Owner: db2018039
--

CREATE TRIGGER cek_level_tambah_poin_trig AFTER UPDATE OF poin ON toys_rent.anggota FOR EACH ROW EXECUTE PROCEDURE toys_rent.cek_level_tambah_poin();


--
-- Name: pemesanan pemesanan_trigger; Type: TRIGGER; Schema: toys_rent; Owner: db2018039
--

CREATE TRIGGER pemesanan_trigger AFTER INSERT OR UPDATE ON toys_rent.pemesanan FOR EACH ROW EXECUTE PROCEDURE toys_rent.update_harga_sewa();


--
-- Name: pengiriman pemesanan_trigger; Type: TRIGGER; Schema: toys_rent; Owner: db2018039
--

CREATE TRIGGER pemesanan_trigger AFTER INSERT OR UPDATE ON toys_rent.pengiriman FOR EACH ROW EXECUTE PROCEDURE toys_rent.update_ongkos();


--
-- Name: level_keanggotaan ubah_data_level_trig; Type: TRIGGER; Schema: toys_rent; Owner: db2018039
--

CREATE TRIGGER ubah_data_level_trig AFTER INSERT OR DELETE OR UPDATE ON toys_rent.level_keanggotaan FOR EACH ROW EXECUTE PROCEDURE toys_rent.ubah_data_level();


--
-- Name: pemesanan update_kondisi_trigger; Type: TRIGGER; Schema: toys_rent; Owner: db2018039
--

CREATE TRIGGER update_kondisi_trigger AFTER INSERT ON toys_rent.pemesanan FOR EACH ROW EXECUTE PROCEDURE toys_rent.update_kondisi();


--
-- Name: admin admin_no_ktp_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.admin
    ADD CONSTRAINT admin_no_ktp_fkey FOREIGN KEY (no_ktp) REFERENCES toys_rent.pengguna(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: alamat alamat_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.alamat
    ADD CONSTRAINT alamat_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota) REFERENCES toys_rent.anggota(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: anggota anggota_level_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.anggota
    ADD CONSTRAINT anggota_level_fkey FOREIGN KEY (level) REFERENCES toys_rent.level_keanggotaan(nama_level) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: anggota anggota_no_ktp_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.anggota
    ADD CONSTRAINT anggota_no_ktp_fkey FOREIGN KEY (no_ktp) REFERENCES toys_rent.pengguna(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_dikembalikan barang_dikembalikan_id_barang_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.barang_dikembalikan
    ADD CONSTRAINT barang_dikembalikan_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES toys_rent.barang(id_barang);


--
-- Name: barang_dikembalikan barang_dikembalikan_no_resi_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.barang_dikembalikan
    ADD CONSTRAINT barang_dikembalikan_no_resi_fkey FOREIGN KEY (no_resi) REFERENCES toys_rent.pengembalian(no_resi);


--
-- Name: barang_dikirim barang_dirikim_id_barang_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.barang_dikirim
    ADD CONSTRAINT barang_dirikim_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES toys_rent.barang(id_barang);


--
-- Name: barang_dikirim barang_dirikim_no_resi_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.barang_dikirim
    ADD CONSTRAINT barang_dirikim_no_resi_fkey FOREIGN KEY (no_resi) REFERENCES toys_rent.pengiriman(no_resi);


--
-- Name: barang barang_nama_item_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.barang
    ADD CONSTRAINT barang_nama_item_fkey FOREIGN KEY (nama_item) REFERENCES toys_rent.item(nama);


--
-- Name: barang barang_no_ktp_penyewa_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.barang
    ADD CONSTRAINT barang_no_ktp_penyewa_fkey FOREIGN KEY (no_ktp_penyewa) REFERENCES toys_rent.anggota(no_ktp);


--
-- Name: barang_pesanan barang_pesanan_id_barang_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.barang_pesanan
    ADD CONSTRAINT barang_pesanan_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES toys_rent.barang(id_barang);


--
-- Name: barang_pesanan barang_pesanan_id_pemesanan_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.barang_pesanan
    ADD CONSTRAINT barang_pesanan_id_pemesanan_fkey FOREIGN KEY (id_pemesanan) REFERENCES toys_rent.pemesanan(id_pemesanan);


--
-- Name: barang_pesanan barang_pesanan_status_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.barang_pesanan
    ADD CONSTRAINT barang_pesanan_status_fkey FOREIGN KEY (status) REFERENCES toys_rent.status(nama);


--
-- Name: chat chat_no_ktp_admin_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.chat
    ADD CONSTRAINT chat_no_ktp_admin_fkey FOREIGN KEY (no_ktp_admin) REFERENCES toys_rent.admin(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: chat chat_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.chat
    ADD CONSTRAINT chat_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota) REFERENCES toys_rent.anggota(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: info_barang_level info_barang_level_id_barang_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.info_barang_level
    ADD CONSTRAINT info_barang_level_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES toys_rent.barang(id_barang);


--
-- Name: info_barang_level info_barang_level_nama_level_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.info_barang_level
    ADD CONSTRAINT info_barang_level_nama_level_fkey FOREIGN KEY (nama_level) REFERENCES toys_rent.level_keanggotaan(nama_level);


--
-- Name: kategori_item kategori_item_nama_item_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.kategori_item
    ADD CONSTRAINT kategori_item_nama_item_fkey FOREIGN KEY (nama_item) REFERENCES toys_rent.item(nama);


--
-- Name: kategori_item kategori_item_nama_kategori_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.kategori_item
    ADD CONSTRAINT kategori_item_nama_kategori_fkey FOREIGN KEY (nama_kategori) REFERENCES toys_rent.kategori(nama);


--
-- Name: kategori kategori_sub_dari_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.kategori
    ADD CONSTRAINT kategori_sub_dari_fkey FOREIGN KEY (sub_dari) REFERENCES toys_rent.kategori(nama);


--
-- Name: pemesanan pemesanan_no_ktp_pemesan_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.pemesanan
    ADD CONSTRAINT pemesanan_no_ktp_pemesan_fkey FOREIGN KEY (no_ktp_pemesan) REFERENCES toys_rent.anggota(no_ktp);


--
-- Name: pemesanan pemesanan_status_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.pemesanan
    ADD CONSTRAINT pemesanan_status_fkey FOREIGN KEY (status) REFERENCES toys_rent.status(nama);


--
-- Name: pengembalian pengembalian_id_pemesanan_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.pengembalian
    ADD CONSTRAINT pengembalian_id_pemesanan_fkey FOREIGN KEY (id_pemesanan) REFERENCES toys_rent.pemesanan(id_pemesanan);


--
-- Name: pengembalian pengembalian_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.pengembalian
    ADD CONSTRAINT pengembalian_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota, nama_alamat_anggota) REFERENCES toys_rent.alamat(no_ktp_anggota, nama);


--
-- Name: pengembalian pengembalian_no_resi_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.pengembalian
    ADD CONSTRAINT pengembalian_no_resi_fkey FOREIGN KEY (no_resi) REFERENCES toys_rent.pengiriman(no_resi);


--
-- Name: pengiriman pengiriman_id_pemesanan_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.pengiriman
    ADD CONSTRAINT pengiriman_id_pemesanan_fkey FOREIGN KEY (id_pemesanan) REFERENCES toys_rent.pemesanan(id_pemesanan);


--
-- Name: pengiriman pengiriman_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: toys_rent; Owner: db2018039
--

ALTER TABLE ONLY toys_rent.pengiriman
    ADD CONSTRAINT pengiriman_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota, nama_alamat_anggota) REFERENCES toys_rent.alamat(no_ktp_anggota, nama);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

