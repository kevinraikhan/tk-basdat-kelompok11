from django.urls import path
from .views import *

app_name = 'review'

urlpatterns = [
    path('buat-review/', buat_review, name = 'buat-review'),
    path('buat-review/<str:no_resi>', buat_review),
    path('update-review/<str:no_resi>/<str:no_urut>', update_review),
    path('update-review/', update_review_post_handler, name = 'update-review'),
    path('daftar-review/', daftar_review, name = 'daftar-review'),
]
