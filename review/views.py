from django.shortcuts import render
from .forms import SingleFieldForm
from django.db import connection
import psycopg2
from django.http import HttpResponse
from datetime import date


def buat_review(req):
    return render(req, 'buat_review.html')

#def buat_review(req, no_resi):
#    with connection.cursor() as cursor:
 #       cursor.execute("SELECT p.no_resi FROM toys_rent.pengiriman WHERE p.no_resi = '"+ no_resi + "'")
 #       pengiriman = cursor.fetchall()
 #   with connection.cursor() as cursor:
  #      cursor.execute("SELECT bp.id_barang, b.nama_item FROM toys_rent.pengiriman p, toys_rent.barang_pesanan bp, toys_rent.barang b WHERE p.id_pemesanan = bp.id_pemesanan AND bp.id_barang = b.id_barang AND p.no_resi = '" + no_resi + "' AND bp.id_barang NOT IN (SELECT bd id_barang FROM toys_rent.barang_dikirim bd")
   #     barang = cursor.fetchall()

 #   return render(req, 'buat_review.html', {'pengiriman' : pengiriman, 'barang' : barang})

def buat_review_post_handler(req):
   if req.method == 'POST':
#        no_resi = req.POST['no_resi']
#        no_urut = ''
#        review = req.POST['review']
#        tgl_review = date.today().strftime("%Y-%m-%d")
#
#        with connection.cursor() as cursor:
#            cursor.execute("UPDATE toys_rent.barang_dikirim SET review = '" + review + "', tanggal_review = '" + tgl_review + "' WHERE no_resi = '" + no_resi + "' AND no_urut = '" + no_urut + "';")
#
#        print(req.POST)
        return daftar_review(req)
#    else:
        return HttpResponse('bukan POST')

def update_review(req, no_resi, no_urut):
    with connection.cursor() as cursor:
        cursor.execute("SELECT bd.no_resi, b.nama_item, bd.tanggal_review, bd.review, bd.no_urut FROM toys_rent.barang_dikirim bd, toys_rent.barang b WHERE b.id_barang = bd.id_barang AND bd.no_resi = '" + no_resi + "' AND bd.no_urut = '" + no_urut + "'")
        barang_dikirim = cursor.fetchall()

    return render(req, 'update_review.html', {'barang_dikirim' : barang_dikirim})

def update_review_post_handler(req):
    if req.method == 'POST':
        no_resi = req.POST['no_resi']
        no_urut = req.POST['no_urut']
        review = req.POST['review']
        tgl_review = date.today().strftime("%Y-%m-%d")

        with connection.cursor() as cursor:
            cursor.execute("UPDATE toys_rent.barang_dikirim SET review = '" + review + "', tanggal_review = '" + tgl_review + "' WHERE no_resi = '" + no_resi + "' AND no_urut = '" + no_urut + "';")

        print(req.POST)
        return daftar_review(req)
    else:
        return HttpResponse('bukan POST')

def daftar_review(req):
    return render(req, 'daftar_review.html', {'list_review': get_review()})

def get_review():
    with connection.cursor() as cursor:
        cursor.execute("SELECT bd.no_resi, b.nama_item, bd.tanggal_review, bd.review, bd.no_urut FROM toys_rent.barang_dikirim bd, toys_rent.barang b WHERE b.id_barang = bd.id_barang")
        data = cursor.fetchall()
    return data

