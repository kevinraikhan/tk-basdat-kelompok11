from django.shortcuts import render
from .forms import SingleFieldForm
from django.db import connection
import psycopg2
from django.http import HttpResponse
import review.views

def buat_pengiriman(req):
    form_buat_pengiriman = SingleFieldForm()
    return render(req, 'buat_pengiriman.html', {'form_buat_pengiriman' : form_buat_pengiriman})

def update_pengiriman(req, no_resi):
    with connection.cursor() as cursor:
        cursor.execute("SELECT p.no_resi, p.id_pemesanan, p.tanggal, p.metode, p.nama_alamat_anggota FROM toys_rent.pengiriman p WHERE p.no_resi = '" + no_resi + "'")
        pengiriman = cursor.fetchone()
    with connection.cursor() as cursor:
        cursor.execute("SELECT bp.id_barang, b.nama_item FROM toys_rent.pengiriman p, toys_rent.barang_pesanan bp, toys_rent.barang b WHERE p.id_pemesanan = bp.id_pemesanan AND bp.id_barang = b.id_barang AND p.no_resi = '" + no_resi + "'")
        barang = cursor.fetchall()
    with connection.cursor() as cursor:
        cursor.execute("SELECT a.nama, a.jalan, a.nomor, a.kota, a.kodepos FROM toys_rent.alamat a, toys_rent.pengiriman p WHERE a.no_ktp_anggota = p.no_ktp_anggota AND p.no_resi = '" + no_resi + "'")
        alamat = cursor.fetchall()

    return render(req, 'update_pengiriman.html', {'pengiriman' : pengiriman, 'barang' : barang, 'alamat' : alamat})

def update_pengiriman_post_handler(req):
    if req.method == 'POST':
        alamat = req.POST['alamat']
        tanggal = req.POST['tanggal']
        metode = req.POST['metode']
        no_resi = req.POST['no_resi']

        with connection.cursor() as cursor:
            cursor.execute("UPDATE toys_rent.pengiriman SET nama_alamat_anggota = '" + alamat + "', metode = '" + metode + "', tanggal = '" + tanggal + "' WHERE no_resi = '" + no_resi + "';")

        print(req.POST)
        return daftar_pengiriman(req)
    else:
        return HttpResponse('bukan POST')

def daftar_pengiriman(req):
    return render(req, 'daftar_pengiriman.html', {'list_pengiriman': get_pengiriman()})

def get_pengiriman():
    with connection.cursor() as cursor:
        cursor.execute("SELECT p.no_resi, b.nama_item, p.id_pemesanan, p.ongkos, p.tanggal FROM toys_rent.pengiriman p, toys_rent.barang_pesanan bp, toys_rent.barang b WHERE p.id_pemesanan = bp.id_pemesanan AND bp.id_barang = b.id_barang")
        data = cursor.fetchall()
    return data


