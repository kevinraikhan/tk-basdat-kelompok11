from django.urls import path
from .views import *

app_name = 'pengiriman'

urlpatterns = [
    path('buat-pengiriman/', buat_pengiriman, name = 'buat-pengiriman'),
    path('update-pengiriman/<str:no_resi>/', update_pengiriman),
    path('update-pengiriman/', update_pengiriman_post_handler, name = 'update-pengiriman'),
    path('daftar-pengiriman/', daftar_pengiriman, name = 'daftar-pengiriman'),
    

]
