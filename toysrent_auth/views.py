from datetime import datetime
from django.db import connection
from django.http import JsonResponse
from django.shortcuts import render
from .forms import SingleFieldForm
from item.models import Kategori
import psycopg2
import json

def dictfetchall(cursor):
    "Returns all rows from a cursor as a dict"
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]

def get_admins():
    with connection.cursor() as control:
        control.execute("select * from toys_rent.admin natural join toys_rent.pengguna")
        return control.fetchall()

def get_members():
    with connection.cursor() as control:
        control.execute("select * from toys_rent.anggota natural join toys_rent.pengguna")
        return control.fetchall()

def get_addresses():
    with connection.cursor() as control:
        control.execute("select * from toys_rent.alamat")
        return control.fetchall()

def get_categories():
    with connection.cursor() as control:
        control.execute("select * from toys_rent.kategori")
        return dictfetchall(control)

def register(req):
    req.session.flush()
    flags = {'id_exist': False, 'email_exist': False}

    if req.method == 'POST' and req.is_ajax():
        data = json.loads(req.body)
        
        idn = data['global_data'][0]
        name = data['global_data'][1]
        email = data['global_data'][2]
        bdate = datetime.strptime(data['global_data'][3], "%m/%d/%Y")
        telp = data['global_data'][4]

        if data['role'] == "Admin":
            admins = get_admins()
            for admin in admins:
                if idn in admin:
                    flags['id_exist'] = True
                    return JsonResponse(flags)
                
                if email in admin:
                    flags['email_exist'] = True
                    return JsonResponse(flags)
            
            with connection.cursor() as control:
                control.execute("insert into toys_rent.pengguna(no_ktp, nama_lengkap, email, tanggal_lahir, no_telp)" +
                                "values(%s, %s, %s, %s, %s)", (idn, name, email, bdate, telp,))
                control.execute("insert into toys_rent.admin(no_ktp) values (%s)", (idn,))

                return JsonResponse(flags)
        else:
            members = get_members()
            addresses = data['global_data'][5]

            for member in members:
                if idn in member:
                    flags['id_exist'] = True
                    return JsonResponse(flags)
                
                if email in member:
                    flags['email_exist'] = True
                    return JsonResponse(flags)
            
            with connection.cursor() as control:
                control.execute("insert into toys_rent.pengguna(no_ktp, nama_lengkap, email, tanggal_lahir, no_telp)" +
                                "values(%s, %s, %s, %s, %s)", (idn, name, email, bdate, telp,))
                control.execute("insert into toys_rent.anggota(no_ktp, poin, level) values (%s, %s, %s)", (idn, 0, "BRONZE",))

                for address in addresses:
                    control.execute("insert into toys_rent.alamat(no_ktp_anggota, nama, jalan, nomor, kota, kodepos)" +
                                    "values(%s, %s, %s, %s, %s, %s)", (idn, address[0], address[1], int(address[2]), address[3], address[4],))
                return JsonResponse(flags)
                     
    else:
        form_register_pengguna = SingleFieldForm()
        return render(req, 'register.html', {'form_register_pengguna' : form_register_pengguna, 'admins' : get_admins()})

def init_categories():
    categories = get_categories()

    for category in categories:
        kategori = Kategori()
        kategori.nama = category['nama']
        kategori.level = int(category['level'])
        kategori.save()

def login(req):
    req.session.flush()
    flags = {'id_exist': False, 'mismatch': True, 'is_admin': False}

    if req.method == 'POST' and req.is_ajax():
        init_categories()
        data = json.loads(req.body)
        admins = get_admins()
        members = get_members()

        idn = data['global_login'][0]
        email = data['global_login'][1]

        for admin in admins:
            if idn in admin:
                flags['id_exist'] = True
                flags['is_admin'] = True
                if email in admin:
                    flags['mismatch'] = False
                    req.session['role'] = "admin"
                    req.session['idn'] = idn
                return JsonResponse(flags)

        for member in members:
            if idn in member:
                flags['id_exist'] = True
                if email in member:
                    flags['mismatch'] = False
                    req.session['role'] = "member"
                    req.session['idn'] = idn
                return JsonResponse(flags)
        return JsonResponse(flags)

    else:
        form_login = SingleFieldForm()
        return render(req, 'login.html', {'form_login' : form_login})