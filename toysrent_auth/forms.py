from django import forms

class SingleFieldForm(forms.Form):
    single_char_field = forms.CharField(label = "", max_length = 50, widget = forms.TextInput(
        attrs = {
            'class': 'form-field',
            'placeholder': '',
            'required': True,
        }
    ))
    
    date_field = forms.DateField(widget = forms.DateInput(
        attrs = {
            'class': 'form-field datepicker',
            'placeholder': '',
            'required': True,
        }
    ))

    date_field_2 = forms.DateField(widget = forms.DateInput(
        attrs = {
            'class': 'form-field datepicker2',
            'placeholder': '',
            'required': True,
        }
    ))
    
