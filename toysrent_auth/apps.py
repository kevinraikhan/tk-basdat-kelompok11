from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = 'toysrent_auth'
