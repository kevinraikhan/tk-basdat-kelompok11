from django.urls import path
from .views import *

app_name = 'toysrent_auth'

urlpatterns = [
    path('register/', register, name = 'register'),
    path('login/', login, name = 'login'),

]
