from django.db import connection
from django.http import HttpResponse
from django.shortcuts import render


def get_all_level():
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM toys_rent.LEVEL_KEANGGOTAAN")
        data = cursor.fetchall()

    return data


# Create your views here.
def index(request):
    if request.method == 'POST':

        nama_level = request.POST['nama_level']
        print(nama_level)
        with connection.cursor() as cursor:
            cursor.execute("SET search_path TO toys_rent")
            cursor.execute("DELETE FROM LEVEL_KEANGGOTAAN WHERE LOWER(nama_level) = LOWER('{}');".format(nama_level))

        return HttpResponse('Success')

    else:
        print('\n>>>Session:')
        print(request.session.keys())
        print('\n')
        context = {
            'list_level': get_all_level(),
            'idn': request.session.get('idn'),
            'role': request.session.get('role')
        }
        # print('idn: ' + request.session.get('idn')+ " role:" + request.session.get('role'))
        return render(request, 'daftar-level.html', context)


def tambah_level(request):
    if request.method == 'POST':
        min_poin = request.POST['minimum_poin']
        desc = request.POST['desc']
        nama_level = request.POST['nama_level']

        with connection.cursor() as cursor:
            cursor.execute("SET search_path TO toys_rent")
            cursor.execute("INSERT INTO LEVEL_KEANGGOTAAN VALUES ('{}',{},'{}');".format(nama_level, min_poin, desc))

        return render(request, 'berhasil-tambah-level.html')
    else:
        return render(request, 'tambah-level.html')


def update_level_page(request, nama_level):
    nama_level = str(nama_level).lower()
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM toys_rent.LEVEL_KEANGGOTAAN WHERE LOWER(nama_level) = '" + nama_level + "'")
        level = cursor.fetchone()

    context = {
        'level': level,
    }

    return render(request, 'update-level.html', context)


def update_post_handler(request):
    print("KEUPDATE")
    if request.method == 'POST':
        min_poin = request.POST['minimum_poin']
        desc = request.POST['desc']
        nama_level = request.POST['nama_level']
        print(nama_level)
        # print(request.POST)

        with connection.cursor() as cursor:
            cursor.execute("SET search_path TO toys_rent")
            cursor.execute("UPDATE LEVEL_KEANGGOTAAN SET minimum_poin = {}, deskripsi = '{}' WHERE nama_level = '{}'".format(min_poin, desc, nama_level))
                # "UPDATE LEVEL_KEANGGOTAAN SET minimum_poin = 3987" + ", deskripsi = '" + desc + "' WHERE nama_level = '" + nama_level + "';")


        print(request.POST)
        return HttpResponse('berhasil POST')
    else:
        return HttpResponse('bukan POST')
