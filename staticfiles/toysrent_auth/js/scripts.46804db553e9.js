$(document).ready(function() {
    $('.form-field').attr("placeholder", "Nomor KTP");
    $('.active').css("color", "#007bff");

    $('.next-button:visible').keydown(function(event){
        if (event.keyCode == 13) {
            event.preventDefault();
            submitAction(this);
        }
        
    });

    $('#role-admin').click(function() {
        $('#role-dropdown').text("Admin");
        $('#reg-content-admin').show();
        $('#reg-content-anggota').hide();
    });

    $('#role-anggota').click(function() {
        $('#role-dropdown').text("Anggota");
        $('#reg-content-anggota').show();
        $('#reg-content-admin').hide();
    });
});

function submitAction(element) {
    event.preventDefault();
    let currentActive = $('.active:visible');
    let state = $('.active:visible').attr('value');
    console.log(state);

    if (state !== "final") {
        let nextId = parseInt(currentActive.attr('class').match(/\d+/)[0]) + 1;
        let nextActive = $("." + nextId.toString() + ":visible");

        console.log(nextId);
        console.log(nextActive);
        
        currentActive.css("color", "black");
        currentActive.toggleClass("active");
        currentActive.toggleClass("done");
        nextActive.css("color", "#007bff");
        nextActive.toggleClass("active");

        $('.form-field:visible').attr("placeholder", nextActive.text());
        $('.form-field:visible').val(null);

        if(nextActive.text() === "Alamat") {
            $('.plus-button').show();
        }
        
    } else {
        $(element).text("✓");
        $(element).css("color", "#07ad3c");
    }
}

let countAddress = 1
function addAddress() {
    countAddress++;
    let $lastAddressClone = $(".last-address");
    let $addressClone = $(".last-address").clone();
    $lastAddressClone.toggleClass("last-address");
    $addressClone.addClass("last-address");
    $addressClone.attr("id", "form-" + countAddress);
    $addressClone.children('#id_single_char_field').attr("placeholder", "Alamat " + countAddress);
    $lastAddressClone.children('.plus-button').hide();
    $addressClone.children('.plus-button').show();
    $addressClone.children('.form-field').val(null);

    $('#reg-content-anggota').append($addressClone);
}