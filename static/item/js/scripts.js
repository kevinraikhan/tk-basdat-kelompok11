$(document).ready(function () {
  $('#dtItem').DataTable();
  $('.dataTables_length').addClass('bs-select');
});

function deleteItem(element) {
  let itemId = $(element).attr('id').split("-")[1];
  let itemName = $(element).attr('id').split("-")[2];

  $.ajax({
    method: 'POST',
    url:'/items/delete/',
    dataType: 'json',
    data: JSON.stringify(
        {
            'item_name' : itemName,
        }
    ),
    beforeSend: function(request) {
        request.setRequestHeader('X-CSRFToken', $('input[name="csrfmiddlewaretoken"]').attr('value'));
        $("body").LoadingOverlay("show", {
            background : "rgba(255, 255, 255, 0.7)",
            fade: [0, 200],
        });
    },
    success : function(response) {
      if (response.success) {
        $("body").LoadingOverlay("hide");
        $("#row-" + itemId).remove();
        $('#after-modal').click();
      }
    }
  })
}