const dev = false;
var initLogin;
setTimeout(function(){
    initLogin = $(document.body).html();
}, 2000); 

$(document).ready(function() {

    $('.next-button:visible').keydown(function(event){
        if (event.keyCode == 13) {
            event.preventDefault();
        }      
    });

    $('#role-admin').click(function() {
        $('#role-dropdown').text("Admin");
        $('#role-dropdown').attr("disabled", true);
        $('#reg-content-admin').show();
        $('#reg-content-anggota').hide();
        $('.form-field:visible').attr("placeholder", $('.active:visible').text());
    
        
    });

    $('#role-anggota').click(function() {
        $('#role-dropdown').text("Anggota");
        $('#role-dropdown').attr("disabled", true);
        $('#reg-content-anggota').show();
        $('#reg-content-admin').hide();
        $('.form-field:visible').attr("placeholder", $('.active:visible').text());
    });
});

var globalAdmin = [];
var globalAnggota = [];

function submitActionRegister(element) {
    event.preventDefault();
    let currentActive = $('.active:visible');
    let state = $('.active:visible').attr('value');

    if (state !== "final") {
        if ($('#role-dropdown').text() === "Admin") {
            if ($('.form-field:visible').val() !== "") globalAdmin.push($('.form-field:visible').val());
            if ($('.hasDatepicker:visible').val() !== undefined) {
                globalAdmin.push($('.hasDatepicker:visible').val());
                globalAdmin.pop();
            }
            if ($('.form-field:visible').val() === "" && $('.hasDatepicker:visible').val() === undefined) {
                alert("Please fill all blanks");
                return false;
            }
        } else {
            if ($('.form-field:visible').val() !== "") globalAnggota.push($('.form-field:visible').val());
            if ($('.hasDatepicker:visible').val() !== undefined) {
                globalAnggota.push($('.hasDatepicker:visible').val());
                globalAnggota.pop();
            }
            if ($('.form-field:visible').val() === "" && $('.hasDatepicker:visible').val() === undefined) {
                alert("Please fill all blanks");
                return false;
            }
        }

        $('.form-field').show();
        $('.hasDatepicker:visible').hide();

        let nextId = parseInt(currentActive.attr('class').match(/\d+/)[0]) + 1;
        let nextActive = $("." + nextId.toString() + ":visible");

        
        currentActive.css("color", "black");
        currentActive.toggleClass("active");
        currentActive.toggleClass("done");
        nextActive.css("color", "#007bff");
        nextActive.toggleClass("active");

        $('.form-field:visible').attr("placeholder", nextActive.text());
        $('.form-field:visible').val(null);

        if(nextActive.text() === "Alamat") {
            $('.form-field:visible').hide();
            $('.plus-button').show();
            $('.alamat').show();
            $('#nama > .form-field:visible').attr("placeholder", "Nama");
            $('#jalan > .form-field:visible').attr("placeholder", "Jalan");
            $('#nomor > .form-field:visible').attr("placeholder", "Nomor");
            $('#kota > .form-field:visible').attr("placeholder", "Kota");
            $('#kodepos > .form-field:visible').attr("placeholder", "Kode Pos");

        }

        if(nextActive.text() === "Tanggal Lahir") {
            $('.hasDatepicker').attr("placeholder", "Klik untuk memilih tanggal");
            $('.form-field:visible').hide();
            $('.hasDatepicker').show();
        }
        
    } else {
        let role = $('#role-dropdown').text();
        let flagFilled = true;
        if (role === "Admin") {
            if ($('.form-field:visible').val() !== "") globalAdmin.push($('.form-field:visible').val());
            else {
                alert("Please fill all blanks");
                flagFilled = false;
            }
        } else {
            $.each($('.login-form:visible'), function() {
                $.each($(this).children('.alamat:visible').children('.form-field:visible'), function() {
                    if ($(this).val() === "") {
                        flagFilled = false;
                        alert("Please fill all blanks");
                        return false;
                    }
                });
            });

        }

        if (flagFilled) {
            temp = []
            $.each($('.login-form:visible'), function() {
                temp.push([])
                $.each($(this).children('.alamat:visible').children('.form-field:visible'), function() {
                    if ($(this).val() !== "") temp[temp.length -1].push($(this).val());
                    
                });
            });
            globalAnggota.push(temp);

            $.ajax({
                method: 'POST',
                url: dev ? 'http://localhost:8000/auth/register/' : 'https://toysrent-basdat.herokuapp.com/auth/register/',
                dataType: 'json',
                data: JSON.stringify(
                    {
                        'global_data': role === "Admin" ? globalAdmin : globalAnggota,
                        'role': role,
                    }
                ),
                beforeSend: function(request) {
                    request.setRequestHeader('X-CSRFToken', $('input[name="csrfmiddlewaretoken"]').attr('value'));
                    $("body").LoadingOverlay("show", {
                        background : "rgba(255, 255, 255, 0.7)",
                        fade: [0, 200],
                    });
                },
                success : function(response) {
                    $("body").LoadingOverlay("hide");
                    
                    if (response.email_exist || response.id_exist) {
                        $('.warn-msg').show();
    
                        setTimeout(function(){
                            location.reload();
                        }, 1000);      
                    }
                    else {
                        $('#success-reg').click();
                        $(element).text("✓");
                        $(element).css("color", "#07ad3c");
                    }
                }
            })
    
        }  
    }
}

var globalLogin = []
function submitActionLogin(element) {
    //TODO
    event.preventDefault();
    let currentActive = $('.active:visible');
    let state = $('.active:visible').attr('value');

    if (state !== "final") {
        if ($('.form-field:visible').val() !== "") globalLogin.push($('.form-field:visible').val());
        else {
            alert("Please fill all blanks");
            return false;
        }

        let nextId = parseInt(currentActive.attr('class').match(/\d+/)[0]) + 1;
        let nextActive = $("." + nextId.toString() + ":visible");
        
        currentActive.css("color", "black");
        currentActive.toggleClass("active");
        currentActive.toggleClass("done");
        nextActive.css("color", "#007bff");
        nextActive.toggleClass("active");

        $('.form-field:visible').attr("placeholder", nextActive.text());
        $('.form-field:visible').val(null);
        
    } else {
        if ($('.form-field:visible').val() !== "") globalLogin.push($('.form-field:visible').val());
        else {
            alert("Please fill all blanks");
            return false;
        }
        $.ajax({
            method: 'POST',
            url: dev ? 'http://localhost:8000/auth/login/' : 'https://toysrent-basdat.herokuapp.com/auth/login/',
            dataType: 'json',
            data: JSON.stringify(
                {
                    'global_login': globalLogin,
                }
            ),
            beforeSend: function(request) {
                request.setRequestHeader('X-CSRFToken', $('input[name="csrfmiddlewaretoken"]').attr('value'));
                $("body").LoadingOverlay("show", {
                    background : "rgba(255, 255, 255, 0.7)",
                    fade: [0, 200],
                });
            },
            success : function(response) {
                
                if (response.id_exist && response.mismatch) {
                    $(document.body).html(initLogin);
                    globalLogin = []
                    $('.warn-msg').text("Nomor KTP anda yang terdaftar tidak sesuai dengan E-mail yang Anda masukkan.")
                    $('.warn-msg').show();

                       
                } else if (!response.id_exist) {
                    $(document.body).html(initLogin);
                    globalLogin = []
                    $('.warn-msg').text("Nomor KTP anda belum terdaftar!")
                    $('.warn-msg').show();
                }
                else if (response.id_exist && !response.mismatch){
                    $(element).text("✓");
                    $(element).css("color", "#07ad3c");

                    if (response.is_admin) window.location.replace(dev ? "http://localhost:8000/pengiriman/daftar-pengiriman/" : "https://toysrent-basdat.herokuapp.com/pengiriman/daftar-pengiriman/");
                    else window.location.replace(dev ? "http://localhost:8000/user/profile/" : "https://toysrent-basdat.herokuapp.com/user/profile/");
                }
            }
        })
    }

}
let countAddress = 1
function addAddress() {
    countAddress++;
    let $lastAddressClone = $(".last-address");
    let $addressClone = $(".last-address").clone();
    $lastAddressClone.toggleClass("last-address");
    $addressClone.addClass("last-address");
    $addressClone.attr("id", "form-" + countAddress);
    $lastAddressClone.children('.plus-button').hide();
    $lastAddressClone.children('#nexty').hide();
    $addressClone.children('.plus-button').show();
    $addressClone.children('.alamat').children('.form-field').val(null);

    $('#reg-content-anggota').append($addressClone);
}