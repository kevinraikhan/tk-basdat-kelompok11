from django.db import connection
from django.shortcuts import render, redirect
import psycopg2

def get_admin(idn):
    with connection.cursor() as control:
        control.execute("select * from toys_rent.admin natural join toys_rent.pengguna where no_ktp = %s", (idn,))
        return control.fetchall()

def get_member(idn):
    with connection.cursor() as control:
        control.execute("select * from toys_rent.anggota natural join toys_rent.pengguna where no_ktp = %s", (idn,))
        return control.fetchall()

def get_addresses(idn):
    with connection.cursor() as control:
        control.execute("select * from toys_rent.alamat where no_ktp_anggota = %s", (idn,))
        return control.fetchall()

def profile(req):
    # Kalo lom login
    if 'role' not in req.session:
        return redirect("toysrent_auth:login")

    address = None
    data_processed = {}

    if (req.session['role'] == "admin"):
        data = get_admin(req.session['idn'])
        data_processed['idn'] = data[0][0]
        data_processed['name'] = data[0][1]
        data_processed['email'] = data[0][2]
        data_processed['bdate'] = data[0][3]
        data_processed['telp'] = data[0][4]
    else:
        data = get_member(req.session['idn'])
        data_processed['idn'] = data[0][0]
        data_processed['name'] = data[0][3]
        data_processed['email'] = data[0][4]
        data_processed['bdate'] = data[0][5]
        data_processed['telp'] = data[0][6]
        data_processed['level'] = data[0][2]
        data_processed['point'] = data[0][1]

        address = get_addresses(req.session['idn'])
    
    return render(req, 'profile.html', {"role" : req.session['role'], 'user_data': data_processed, 'addresses': address})